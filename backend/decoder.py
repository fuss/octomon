import M2Crypto as m2
from zipfile import ZipFile, ZIP_DEFLATED
from gzip import GzipFile
from StringIO import StringIO
import csv

class Decoder:
    """
    Decode the data packet sent by the OctoMon sender
    """
    def __init__(self, pubsrvkey, privsrvkey):
        """
        Initialise a decoder object using the given key
        """
        #Rand.load_file('randpool.dat', -1)

        self.smime = m2.SMIME.SMIME()

        # Load private key and cert.
        self.smime.load_key(privsrvkey, pubsrvkey)

    def __del__(self):
        # Save the PRNG's state.
        #Rand.save_file('randpool.dat')
        pass

    def decode(self, buf):
        """
        Decrypt the buffer
        """
        # Wrap in a BIO
        encrypted = m2.BIO.MemoryBuffer(buf)
        # Load the encrypted data
        p7, data = m2.SMIME.smime_load_pkcs7_bio(encrypted)
        # Decrypt p7
        return self.smime.decrypt(p7)

if __name__ == "__main__":
    import sys
    from message import Message

    dec = Decoder("certs/server.pem", "certs/server_key.pem")
    msg = Message(dec.decode(open(sys.argv[1], "rb").read()))

    print "File:", sys.argv[1]
    print "Fingerprint:", msg.fingerprint()
    print "Metadata:"
    for line in msg.fdget("metadata.txt"):
        print "  ", line.strip()

    def tuples(msg):
        """
        Generate the (host, key, value, timestamp) tuples of the message data
        """
        for host, key, val, ts in csv.reader(msg.fdget("data.csv")):
            yield host, key, val, float(ts)

    for i, t in enumerate(tuples(msg)):
        if i > 10:
            break
        print t
        
    sys.exit(0)
