import M2Crypto as m2
from StringIO import StringIO
from fingerprint import cert_fingerprint

class Encoder:
    """
    Create a data packet to send to the OctoMon server
    """
    def __init__(self, pubsrvkey, pubclnkey, privclnkey):
        """
        Initialise an encoder using the given keys
        """
        #Rand.load_file('randpool.dat', -1)

        self.smime = m2.SMIME.SMIME()

        # Load target cert to encrypt the message to.
        x509 = m2.X509.load_cert(pubsrvkey)
        sk = m2.X509.X509_Stack()
        sk.push(x509)
        self.smime.set_x509_stack(sk)

        # Set cipher: 3-key triple-DES in CBC mode.
        self.smime.set_cipher(m2.SMIME.Cipher('des_ede3_cbc'))

        # Load signer's key and cert.
        self.smime.load_key(privclnkey, pubclnkey)

    def __del__(self):
        # Save the PRNG's state.
        #Rand.save_file('randpool.dat')
        pass

    def _encrypt(self, buffer):
        """
        Encrypt the given buffer (passed as a string) and return the encrypted
        string in SMIME PKCS7 format
        """
        cleartext = m2.BIO.MemoryBuffer(buffer)
        p7 = self.smime.encrypt(cleartext, m2.SMIME.PKCS7_BINARY)
        encrypted = m2.BIO.MemoryBuffer()
        self.smime.write(encrypted, p7)
        return encrypted.read()

    def _sign(self, buffer):
        """
        Sign the given buffer (passed as a string) and return the
        signed message in SMIME PKCS7 format
        """
        cleartext = m2.BIO.MemoryBuffer(buffer)
        p7 = self.smime.sign(cleartext, m2.SMIME.PKCS7_BINARY | m2.SMIME.PKCS7_NOCERTS)
        signed = m2.BIO.MemoryBuffer()
        self.smime.write(signed, p7)
        return signed.read()

    def fingerprint(self, md='sha1'):
        """
        Get the fingerprint of the client public key
        """
        return cert_fingerprint(self.smime.x509, md)

    def cleartext(self, message):
        """
        Encode the message and return the cleartext version
        """
        return message.encode(self._sign)

    def encrypted(self, message):
        """
        Encode and encrypt a message, and return the data in a buffer
        """
        return self._encrypt(self.cleartext(message))

if __name__ == "__main__":
    import sys
    from message import Message

    # Create the encoder with the given crypto keys
    encoder = Encoder('certs/server.pem', 'certs/client.pem', 'certs/client_key.pem')

    buf = StringIO()
    print >>buf, "School: test"
    print >>buf, "Version: test"

    message = Message()
    message.setCertificate(open("certs/client.pem").read())
    message["metadata.txt"] = buf.getvalue()

    buf = StringIO()
    print >>buf, "host1.name, name1, value1, 123456.123"
    print >>buf, "host2.name, name2, value2, 123456.123"
    message["data.csv"] = buf.getvalue()

    print(encoder.encode(message))

    sys.exit(0)
