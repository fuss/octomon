from M2Crypto import ASN1, BIO, Err, EVP, util

def cert_fingerprint(cert, md='md5'):
    if getattr(cert, "get_fingerprint", None) != None:
        # Use the proper way if possible
        return cert.get_fingerprint(md)
    else:
        # If not possible, use the backported way
        der = cert.as_der()
        md = EVP.MessageDigest(md)
        md.update(der)
        digest = md.final()
        return hex(util.octx_to_num(digest))[2:-1].upper()
