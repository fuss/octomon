import M2Crypto as m2
from zipfile import ZipFile, ZIP_DEFLATED
from StringIO import StringIO
from gzip import GzipFile
from UserDict import DictMixin
from fingerprint import cert_fingerprint

class Message(DictMixin):
    """
    Encrypted and signed data packet
    """
    def __init__(self, zipdata=None):
        """
        Initialise an empty message, or with the given zip data
        """
        self._gzdata = dict()
        self.__contains__ = self._gzdata.__contains__
        self.has_key = self.__contains__
        self.__delitem__ = self._gzdata.__delitem__
        self.__iter__ = self._gzdata.__iter__
        self.iterkeys = self.__iter__
        if zipdata:
            self.decode(zipdata)

    def _compress(self, buffer, filename=None):
        """
        Create a gzip file with the buffer contents.  Return the gzip
        file as a string.
        """
        buf = StringIO()
        gzbuf = GzipFile(filename, "w", 9, buf)
        gzbuf.write(buffer)
        gzbuf.close()
        return buf.getvalue()

    def _verify_buf(self, buf):
        """
        Verify the content of a signed buffer, returning the cleartext content
        of the signed message
        """
        smime = m2.SMIME.SMIME()

        # Load the signer's cert.
        x509 = m2.X509.load_cert_string(self.certificate())
        sk = m2.X509.X509_Stack()
        sk.push(x509)
        smime.set_x509_stack(sk)

        # Empty trusted key store, since we use NOVERIFY|NOINTERN
        st = m2.X509.X509_Store()
        smime.set_x509_store(st)
        
        # Load the data, verify it.
        signedbuf = m2.BIO.MemoryBuffer(buf)
        p7, data = m2.SMIME.smime_load_pkcs7_bio(signedbuf)
        return smime.verify(p7, None, m2.SMIME.PKCS7_NOVERIFY | m2.SMIME.PKCS7_NOINTERN)

    def decode(self, zipdata):
        self._gzdata.clear()
        zipdata = StringIO(zipdata)
        zip = ZipFile(zipdata, "r")
        self._certificate = zip.read("pubkey.pem")
        for name in zip.namelist():
            if name.endswith(".gz.smime"):
                self._gzdata[name[:-9]] = self._verify_buf(zip.read(name))

    def gzget(self, key):
        """
        Get the gzipped data for a key
        """
        return self._gzdata[key]

    def fdget(self, key):
        """
        Get the data for a key, as an open file descriptor
        """
        return GzipFile(fileobj=StringIO(self.gzget(key)), mode="rb")

    def __getitem__(self, key):
        return self.fdget(key).read()

    def __setitem__(self, key, value):
        self._gzdata[key] = self._compress(value, key)

    def certificate(self):
        """
        Get the client public key from the message
        """
        return self._certificate

    def setCertificate(self, certdata):
        """
        Set the client public key from the message
        """
        self._certificate = certdata

    def fingerprint(self, md='sha1'):
        """
        Get the fingerprint of the client public key from the message
        """
        return cert_fingerprint(m2.X509.load_cert_string(self.certificate()), md)

    def encode(self, signfunc):
        """
        Build the zip data for this message, using the given signing function.
        Return the unencrypted zip data
        """
        zipdata = StringIO()
        zip = ZipFile(zipdata, "w")
        zip.writestr("pubkey.pem", self._certificate)
        for k, v in self._gzdata.iteritems():
            zip.writestr(k + ".gz.smime", signfunc(v))
        zip.close();
        return zipdata.getvalue()

    def __eq__(self, dec):
        """
        Return true if the content of this message is the same as dec
        """
        return self.certificate() == dec.certificate() and self._gzdata == dec._gzdata
