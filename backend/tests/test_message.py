import unittest
from octomon import Decoder, Encoder, Message
from StringIO import StringIO
import csv

class TestMessage(unittest.TestCase):
    def setUp(self):
        # Create the encoder and the decoder with the right keys
        self.encoder = Encoder('certs/server.pem', 'certs/client.pem', 'certs/client_key.pem')

        # Create the encoder and the decoder with the right keys
        self.decoder = Decoder("certs/server.pem", "certs/server_key.pem")

    def makeTestMessage(self):
        msg = Message()
        msg.setCertificate(open("certs/client.pem").read())

        buf = StringIO()
        print >>buf, "School: test"
        print >>buf, "Version: test"
        msg["metadata.txt"] = buf.getvalue()

        buf = StringIO()
        print >>buf, "host1,name1,value1,123.456"
        print >>buf, "host2,name2,value2,234.567"
        msg["data.csv"] = buf.getvalue()

        return msg

    def makeEncodedData(self):
        return self.encoder.encrypted(self.makeTestMessage())

    def testCertificate(self):
        msg = Message()
        msg.setCertificate("ciao")
        self.assertEquals(msg.certificate(), "ciao")

        encoded = self.encoder.encrypted(msg)
        msg1 = Message(self.decoder.decode(encoded))
        self.assertEquals(msg1.certificate(), "ciao")

    def testTransmission(self):
        # Build a message, encode it and decode it
        msg = self.makeTestMessage()
        encoded = self.encoder.encrypted(msg)
        msg1 = Message(self.decoder.decode(encoded))

        # The fingerprint must match
        self.assertEquals(msg1.fingerprint(), msg.fingerprint())

        # Check that the metadata is the same
        lines = [x for x in msg1.fdget("metadata.txt")]
        self.assertEquals(len(lines), 2)
        self.assertEquals(lines[0].strip(), "School: test")
        self.assertEquals(lines[1].strip(), "Version: test")

        def tuples(msg):
            """
            Generate the (host, key, value, timestamp) tuples of the message data
            """
            for host, key, val, ts in csv.reader(msg.fdget("data.csv")):
                yield host, key, val, float(ts)

        # Check that the data is the same
        tuples = [x for x in tuples(msg1)]
        self.assertEquals(len(tuples), 2)
        self.assertEquals(tuples[0], ("host1", "name1", "value1", 123.456))
        self.assertEquals(tuples[1], ("host2", "name2", "value2", 234.567))

    def testWrongEncryption(self):
        # Create another decoder with the wrong keys
        decoder = Decoder("certs/client.pem", "certs/client_key.pem")

        encoded = self.makeEncodedData()

        # Decoding should fail
        import M2Crypto as m2
        self.assertRaises(m2.SMIME.PKCS7_Error, decoder.decode, encoded)

    def testWrongSignatures(self):
        msg = self.makeTestMessage()
        myfpr = msg.fingerprint()

        # Patch the message to include a wrong public key
        msg.setCertificate(open("certs/server.pem").read())

        encoded = self.encoder.encrypted(msg)

        msg1 = Message()
        decrypted = self.decoder.decode(encoded)
        # Decoding should not succeed
        import M2Crypto as m2
        self.assertRaises(m2.SMIME.PKCS7_Error, msg1.decode, decrypted)
