import unittest
from octomon import Queue, Encoder, Decoder, Message
from tempfile import mkdtemp
from shutil import rmtree
from StringIO import StringIO
from gzip import GzipFile

class TestQueue(unittest.TestCase):
    def setUp(self):
        # Create the encoder and the decoder with the right keys
        self.encoder = Encoder('certs/server.pem', 'certs/client.pem', 'certs/client_key.pem')
        self.decoder = Decoder("certs/server.pem", "certs/server_key.pem")

        self.dirname = mkdtemp()
        self.queue = Queue(self.dirname)

    def tearDown(self):
        rmtree(self.dirname)

    def makeTestMessage(self):
        msg = Message()

        msg.setCertificate(open("certs/client.pem").read())

        buf = StringIO()
        print >>buf, "School: test"
        print >>buf, "Version: test"
        msg["metadata.txt"] = buf.getvalue()

        buf = StringIO()
        print >>buf, "host1,name1,value1,123.456"
        print >>buf, "host2,name2,value2,234.567"
        msg["data.csv"] = buf.getvalue()

        return msg
            
    def testEmpty(self):
        self.assertEquals(self.queue.empty(), True)

        names = [x for x in self.queue.list()]
        self.assertEquals(names, [])

        self.assertEquals(self.queue.read("missing"), None)

    def testOne(self):
        # Add an element and make sure that we can read it
        msg = self.makeTestMessage()
        self.queue.add(self.encoder.cleartext(msg))

        self.assertEquals(self.queue.empty(), False)

        names = [x for x in self.queue.list()]
        self.assertEquals(len(names), 1)

        msg1 = Message(self.queue.read(names[0]))
        self.assertEquals(msg1, msg)
        
        self.queue.remove(names[0])
        names = [x for x in self.queue.list()]
        self.assertEquals(names, [])

    def testValidate(self):
        name = self.queue.add("testtesttest")
        self.assertEquals(self.queue.validate(name), True)
        self.assertEquals(self.queue.validate("../"+self.dirname+"/"+name), False)
        self.assertEquals(self.queue.validate("./"+name), False)
        self.assertEquals(self.queue.validate("../../../../etc/passwd"), False)
