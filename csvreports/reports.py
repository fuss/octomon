from io import BytesIO
import csv
import zipfile
import re
from django.utils.translation import ugettext as _

class CSVReport:
    def __init__(self):
        self.zipdata = BytesIO()
        self.zip = zipfile.ZipFile(self.zipdata, "w", zipfile.ZIP_DEFLATED)

    def makeZip(self):
        self.zip.close()
        return self.zipdata.getvalue()

    def clean(self, s):
        if s is None:
            return ""
        try:
            return re.sub("[\r\n]", " ", str(s))
        except UnicodeEncodeError:
            return "(encoding problem)"

    def addActivity(self, school, request):
        startDate = request.POST.get("csv_report_start_date", None)
        endDate = request.POST.get("csv_report_end_date", None)

        # CSV data
        csvdata = BytesIO()
        writer = csv.writer(csvdata)

        # General data
        # writer.writerow([_("School: %s") % self.school.name])
        # writer.writerow([_("Activity report from %s to %s") % (startDate, endDate)])
		# writer.writerow([_("Total activities: %d") % len(data)])

        # Headers
        writer.writerow([_("Start Date"), _("End date"), _("User"), _("Description")])

        # Data
        for x in school.activity_log(None, None, startDate, endDate):
            row = (x.timestamp, x.end_timestamp, x.user_id, x.description)
            writer.writerow([self.clean(r) for r in row])

        self.zip.writestr("activity.csv", csvdata.getvalue())

    def addComputers(self, school, **kw):
        # CSV data
        csvdata = BytesIO()
        writer = csv.writer(csvdata)

        # Headers
        writer.writerow([_("Hostname"), _("Available RAM"), _("Number of boots")])

        # List of PC with available RAM and number of boots
        for c in school.computers:
            try:
                ram = c.component_data('totalram')[0].value
            except IndexError:
                ram = None
            row = [c.hostname, ram, len(c.usage)]
            writer.writerow([self.clean(r) for r in row])

        self.zip.writestr("computers.csv", csvdata.getvalue())
