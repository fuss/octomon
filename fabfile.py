from fabric.api import sudo, cd, env
from fabric.api import execute

env.hosts = ["rosa.fuss.bz.it"]

def production():
    deploy_dir = "/var/www/octomon/"
    with cd(deploy_dir):
        sudo("git pull")
        sudo("./manage.py collectstatic --noinput")
        sudo("./manage.py migrate")
        sudo("./manage.py compilemessages")
        sudo("touch django.wsgi")

def staging():
    deploy_dir = "/var/www/octomon-staging/"
    with cd(deploy_dir):
        sudo("git pull")
        sudo("./manage.py collectstatic --noinput")
        sudo("./manage.py migrate")
        sudo("./manage.py compilemessages")
        sudo("touch django.wsgi")

def deploy_all():
    execute(staging)
    execute(production)
