# coding: utf-8
from django.contrib import admin
from hosts import models as hmodels


admin.site.register(hmodels.Computer)
admin.site.register(hmodels.Component)
admin.site.register(hmodels.HostScrapBook)
admin.site.register(hmodels.Use)
admin.site.register(hmodels.UseNotGood)
