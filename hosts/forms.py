# coding: utf-8
from django import forms
import hosts.models as hmodels


class ComputerForm(forms.ModelForm):
    class Meta:
        model = hmodels.Computer
        fields = (
            'school', 'hostname', 'location',
            'serial_number', 'video_serial_number', 'warranty_expire',
        )


class HostScrapBookForm(forms.ModelForm):
    class Meta:
        model = hmodels.HostScrapBook
        fields = (
            'content', 'user', 'host',
        )
        widgets = {
            'content': forms.Textarea(attrs={'cols': 50, 'rows': 7}),
            'user': forms.HiddenInput(),
            'host': forms.HiddenInput(),
        }
