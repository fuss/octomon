# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('schools', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Component',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=200, blank=True)),
                ('value', models.CharField(default=b'', max_length=200, blank=True)),
                ('timestamp', models.DateTimeField(default=datetime.datetime.now, null=True, blank=True)),
            ],
            options={
                'db_table': 'components',
            },
        ),
        migrations.CreateModel(
            name='Computer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('hostname', models.CharField(max_length=200, verbose_name='Hostname (FQDN)', blank=True)),
                ('serial_number', models.CharField(max_length=200, verbose_name='Serial Number', blank=True)),
                ('video_serial_number', models.CharField(max_length=200, verbose_name='Video Serial Number', blank=True)),
                ('location', models.CharField(max_length=200, verbose_name='Location', blank=True)),
                ('warranty_expire', models.DateField(null=True, verbose_name='Warranty expire on', blank=True)),
                ('dismissed', models.BooleanField(default=False)),
                ('school', models.ForeignKey(related_name='computers', verbose_name='School', to='schools.School')),
            ],
            options={
                'ordering': ('school', 'hostname'),
                'db_table': 'computer',
            },
        ),
        migrations.CreateModel(
            name='HostScrapBook',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.CharField(max_length=2000)),
                ('timestamp', models.DateTimeField(default=datetime.datetime.now, null=True, blank=True)),
                ('host', models.ForeignKey(related_name='scrapbooks', to='hosts.Computer')),
                ('user', models.ForeignKey(related_name='host_scrapbooks', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('host', '-timestamp'),
                'db_table': 'host_scrap_book',
            },
        ),
        migrations.CreateModel(
            name='Use',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(null=True, blank=True)),
                ('host', models.ForeignKey(related_name='uses', to='hosts.Computer')),
            ],
            options={
                'ordering': ('-timestamp',),
                'db_table': 'used',
            },
        ),
        migrations.CreateModel(
            name='UseNotGood',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(null=True, blank=True)),
                ('host', models.ForeignKey(related_name='uses_not_good', to='hosts.Computer')),
            ],
            options={
                'db_table': 'used_not_good',
                'verbose_name_plural': 'uses not good',
            },
        ),
        migrations.AddField(
            model_name='component',
            name='host',
            field=models.ForeignKey(related_name='components', default=None, blank=True, to='hosts.Computer', null=True),
        ),
    ]
