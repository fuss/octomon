# coding: utf-8
import datetime
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User


class Computer(models.Model):
    school = models.ForeignKey('schools.School', related_name='computers',
            verbose_name=_('School'))
    hostname = models.CharField(max_length=200, blank=True,
            verbose_name=_('Hostname (FQDN)'))
    serial_number = models.CharField(max_length=200, blank=True,
            verbose_name=_('Serial Number'))
    video_serial_number = models.CharField(max_length=200, blank=True,
            verbose_name=_('Video Serial Number'))
    location = models.CharField(max_length=200, blank=True,
            verbose_name=_('Location'))
    warranty_expire = models.DateField(null=True, blank=True,
            verbose_name=_('Warranty expires on'))
    # ./manage.py inspectdb gave a models.IntegerField(null=True, blank=True)
    # MySQL has a TINYINT(4). Django stores BooleanFields as TINYINT when the
    # DB doesn't have a BOOLEAN type (like MySQL). For now I use a BooleanField
    # with default=False, which seems plausible.
    dismissed = models.BooleanField(default=False)

    class Meta:
        db_table = 'computer'
        ordering = ('school', 'hostname', )

    def has_problems(self):
        return True if self.get_non_acked_alarms() else False

    def get_all_alarms(self):
        return list(self.alarms.all())

    def get_acked_alarms(self):
        return list(self.alarms.filter(ack=True))

    def get_non_acked_alarms(self):
        return list(self.alarms.filter(ack=False))

    def get_all_tickets(self):
        return list(self.tickets.all())

    def get_open_tickets(self):
        return list(self.tickets.filter(is_open=True))

    def get_closed_tickets(self):
        return list(self.tickets.filter(is_open=False))

    @property
    def last_used(self):
        usage = Use.objects.filter(host=self)
        if usage.count() > 0:
            return usage[0].timestamp
        else:
            return datetime.datetime.now().replace(hour=0, minute=0, second=0)

    @property
    def usage_count(self):
        return Use.objects.filter(host=self).count()

    def get_last_components(self):
        found_components = {}
        for x in self.components.all():
            if x.name not in found_components.keys():
                found_components[x.name] = x
            else:
                if x.timestamp > found_components[x.name].timestamp:
                    found_components[x.name] = x
        items = found_components.values()
        items.sort(cmp=lambda x,y: cmp(x.name, y.name))
        return items

    def get_daily_uses(self):
        data = {}
        for use in self.uses.all():
            day = use.timestamp.date()
            try:
                data[day] += 1
            except:
                data[day] = 1
        return data

    def get_days_of_use(self):
        data = {}
        for use in self.uses.all():
            day = use.timestamp.date()
            data[day] = 1
        return data

    def is_server(self):
        try:
            q = self.components.filter(name="is_server")
            if len(q) > 0:
                return q.order_by("-timestamp")[0].value == "yes"
            raise
        except:
            pass
        return False

    def __unicode__(self):
        return "%s - %s" % (self.school.name, self.hostname)


class Component(models.Model):
    name = models.CharField(max_length=200, blank=True, default='')
    value = models.CharField(max_length=200, blank=True, default='')
    host = models.ForeignKey('Computer', related_name='components', null=True, blank=True, default=None)
    timestamp = models.DateTimeField(default=datetime.datetime.now, null=True, blank=True)

    class Meta:
        db_table = 'components'

    def __unicode__(self):
        return "%s - %s" % (self.host, self.name)


class HostScrapBook(models.Model):
    host = models.ForeignKey('Computer', related_name='scrapbooks')
    user = models.ForeignKey(User, related_name='host_scrapbooks')
    content = models.CharField(max_length=2000)
    timestamp = models.DateTimeField(default=datetime.datetime.now, null=True, blank=True)

    class Meta:
        db_table = 'host_scrap_book'
        ordering = ('host', '-timestamp', )

    def __unicode__(self):
        return "%s - %s" % (self.host.hostname, self.timestamp)


class Use(models.Model):
    host = models.ForeignKey('Computer', related_name='uses')
    timestamp = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'used'
        ordering = ('-timestamp',)

class UseNotGood(models.Model):
    host = models.ForeignKey('Computer', related_name='uses_not_good')
    timestamp = models.DateTimeField(null=True, blank=True)

    class Meta:
        db_table = 'used_not_good'
        verbose_name_plural = 'uses not good'
