from django.conf.urls import patterns, url
from hosts import views

urlpatterns = patterns('',
    url(r'^host/(?P<id>[0-9]+)$', 'hosts.views.host', name="host"),
    url(r'^dismiss/(?P<id>[0-9]+)$', 'hosts.views.dismiss_host', name="dismiss_host"),
    url(r'^reinstate/(?P<id>[0-9]+)$', 'hosts.views.reinstate_host', name="reinstate_host"),
    url(r'^create/$', 'hosts.views.create_host', name="create_host"),
    # The id in add_scrapbook url is the HOST id
    url(r'^add_scrapbook/(?P<id>[0-9]+)$', 'hosts.views.add_scrapbook', name="add_host_scrapbook"),
    url(r'^host/(?P<pk>[0-9]+)/drop$', views.DropHost.as_view(), name="drop_host"),
)
