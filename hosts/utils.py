# -*- coding: utf-8 -*-
# Copyright (c) 2018-2019 Marco Marinello <marco.marinello@fuss.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json


def filter(list, query):
    result = []
    for x in list:
        if query in x.name:
            result.append(x)
    return result


def filter_by_list(list, query):
    result = []
    for x in list:
        if x.name in query:
            result.append(x)
    return result


def to_dict(cList):
    out = {}
    for i in cList:
        out[i.name] = i.value
    return out


def add_info(_disks, disk, info, index):
    disks = []
    ADDED = False
    for i in _disks:
        if i[0] == disk or i[1] == disk:
            i[index] = info
            ADDED = True
        disks.append(i)
    if not ADDED:
        this = [
            "Missing device",
            "Missing mountpoint",
            "missing",
            "missing",
            "missing"
        ]
        if disk.startswith("/dev"):
            this[0] = disk
        else:
            this[1] = disk
        this[index] = info
        disks.append(this)
    return disks


def parse_disks(components):
    parsed_disks = []
    # disks = [disk, ]
    # disk = [ device, mountpoint, size, usage, fstype ]
    disks = []
    for i in filter(components, " mountpoint"):
        disk = i.name.split(" ")[0]
        disks = add_info(disks, disk, i.value, 1)
    for i in components:
        if " usage" in i.name:
            try:
                disk = i.name.split(" ")[0]
                info = i.value
                disks = add_info(disks, disk, info, 3)
            except:
                pass
        elif " size" in i.name:
            try:
                disk = i.name.split(" ")[0]
                info = i.value
                disks = add_info(disks, disk, info, 2)
            except:
                pass
        elif " mountpoint" in i.name:
            try:
                disk = i.name.split(" ")[0]
                info = i.value
                disks = add_info(disks, disk, info, 1)
            except:
                pass
        elif " fstype" in i.name:
            try:
                disk = i.name.split(" ")[0]
                info = i.value
                disks = add_info(disks, disk, info, 4)
            except:
                pass

    return disks


def parse_interfaces(components):
    ips = filter(components, "iface ")
    macs = filter(components, "mac ")

    # ifaces = [ iface, ]
    # iface = [ name, ip, mac ]
    ifaces = []

    for ip in ips:
        ifaces.append([ip.name.split(" ")[1], ip.value, ""])

    for mac in macs:
        _ifaces = ifaces
        ifaces = []
        for x in _ifaces:
            if x[0] == mac.name.split(" ")[1]:
                x[2] = mac.value
            ifaces.append(x)

    return ifaces


def parse_wan_mac(components):
    macs = {}
    wan = None

    for i in components:
        if i.name == "external_iface":
            wan = i.value
        elif "mac " in i.name:
            try:
                macs[i.name.split(" ")[1]] = i.value
            except:
                pass

    try:
        return macs[wan]
    except:
        return ""


def parse_etc(components):
    q = filter(components, "etc_segment")
    this = to_dict(q)
    if "etc_segments" not in this:
        return None
    segments = int(this["etc_segments"])
    j = ""
    for i in range(segments):
        j += this["etc_segment%s" % i]
    data = json.loads(j)
    return data
