# coding: utf-8
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.views.generic.edit import DeleteView
import hosts.models as hmodels
import problems.models as pmodels
import hosts.forms as hforms
import problems.forms as pforms
from hosts import utils
from octomon.mixins import UserMixin


GATHERED_PACKAGES = [
    "fuss-server",
    "fussclient",
    "octofuss-client",
    "octomon-sender",
    "octofussd",
    "octonet"
]


@login_required
def host(request, id):
    host = get_object_or_404(hmodels.Computer, pk=int(id))
    alarmformset = pforms.AlarmFormSet(
        queryset=pmodels.Alarm.objects.filter(host=host, ack=False)
    )
    scrapbookform_initial = {
        'host': host.id,
        'user_id': request.user.id,
    }
    scrapbookform = hforms.HostScrapBookForm(prefix='scrapbookform', initial=scrapbookform_initial)
    if request.method == 'POST' and 'submit_edit_host' in request.POST:
        if not request.user.has_perm('hosts.change_computer'):
            messages.error(request, _("You don't have the permissions to modify this computer"))
            return HttpResponseRedirect(reverse('host', args=[host.id,]))
        # Save the modified computer
        edit_host_form = hforms.ComputerForm(request.POST, instance=host)
        if edit_host_form.is_valid():
            saved_host = edit_host_form.save()
            return redirect('host', id=saved_host.id)
        else:
            # Form not valid
            messages.error(request, _('Computer not saved, form not valid.'))
            return redirect('host', id=id)
    edit_host_form = hforms.ComputerForm(instance=host)
    components = host.get_last_components()
    ctx = {
        'host': host,
        'edit_host_form': edit_host_form,
        'alarmformset': alarmformset,
        'scrapbookform': scrapbookform,
        'disks': utils.parse_disks(components),
        'versions': utils.filter_by_list(components, GATHERED_PACKAGES),
        'interfaces': utils.parse_interfaces(components),
        'lc_wan_mac': utils.parse_wan_mac(components),
        'etc': utils.parse_etc(components)
    }
    for i in components:
        ctx["lc_"+i.name] = i
    return render(request, 'hosts/host.html', ctx)

@login_required
def create_host(request):
    if not request.user.has_perm('hosts.add_computer'):
        messages.error(request, _("You don't have the permission to create a host"))
        return redirect('schools')
    if request.method == 'POST':
        form = hforms.ComputerForm(request.POST)
        if form.is_valid():
            host = form.save()
            messages.info(request, _('Computer %s created successfully' % host.hostname))
            return redirect('schools')
        else:
            messages.error(request, _('Computer not created, form not valid.'))
    else:
        form = hforms.ComputerForm()
        # Filter schools if the user is not superuser
        if not request.user.is_superuser:
            form.fields['school'].queryset = request.user.schools.all()
    return render(request, 'hosts/create_host.html', {
        'form': form,
    })


@login_required
def dismiss_host(request, id):
    host = get_object_or_404(hmodels.Computer, pk=int(id))
    if not request.user.has_perm('hosts.change_computer'):
        messages.error(request, _("You don't have the permission to modify a host"))
        return HttpResponseRedirect(reverse('host', args=[id,]))
    host.dismissed = True
    host.save()
    messages.info(request, _("Host dismissed successfully"))
    return HttpResponseRedirect(reverse('host', args=[host.id,]))


@login_required
def reinstate_host(request, id):
    host = get_object_or_404(hmodels.Computer, pk=int(id))
    if not request.user.has_perm('hosts.change_computer'):
        messages.error(request, _("You don't have the permission to modify a host"))
        return HttpResponseRedirect(reverse('host', args=[id,]))
    host.dismissed = False
    host.save()
    messages.info(request, _("Host reinstated successfully"))
    return HttpResponseRedirect(reverse('host', args=[host.id,]))


class DropHost(UserMixin, SuccessMessageMixin, DeleteView):
    model = hmodels.Computer
    template_name = "hosts/drop_host.html"
    success_message = _("%(hostname)s has been successfully deleted.")

    def get_success_url(self):
        return reverse_lazy("school_detail", kwargs={"pk": self.object.school.pk})


@login_required
def add_scrapbook(request, id):
    # id is the host id
    host = get_object_or_404(hmodels.Computer, pk=int(id))
    if not request.user.has_perm('hosts.add_hostscrapbook'):
        messages.error(request, _("You don't have the permissions to add a scrapbook"))
        return HttpResponseRedirect(reverse('host', args=[host.id,]))
    scrapbookform = hforms.HostScrapBookForm(request.POST, prefix='scrapbookform')
    if scrapbookform.is_valid():
        print "GOOD"
        scrapbook = scrapbookform.save()
    else:
        print "ERROR"
        print scrapbookform.data
        print scrapbookform.errors
        return HttpResponseRedirect(reverse('host', args=[host.id,]))
    return HttpResponseRedirect(reverse('host', args=[scrapbook.host.id,]))
