# coding: utf-8
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        print "Creating 30 active users..."
        for i in range(30):
            johndoe = User(
                username='pippo%02d' % i,
                first_name='John %02d' % i,
                last_name='Doe %02d' % i,
                email='pippo%02d@email.it' % i,
                is_active=True,
            )
            johndoe.set_password(johndoe.username)
            johndoe.save()
        print "Created 30 John Doe"
        # Make the last created user a superuser
        johndoe.is_superuser = True
        johndoe.save()
        print "User with id=%d is superuser" % johndoe.id
