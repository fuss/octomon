from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.exceptions import PermissionDenied

class UserMixin(object):
    perms = []

    def load_objects(self):
        """
        Hook to set self.* members from request parameters, so that they are
        available to the rest of the view members.
        """
        pass

    def check_permissions(self):
        """
        Raise PermissionDenied if some of the permissions requested by the view
        configuration are not met.

        Subclasses can extend this to check their own permissions.
        """
        # the user must be authenticated
        if not self.request.user.is_authenticated:
            raise PermissionDenied

        # The current user must be active
        if not self.request.user.is_active:
            raise PermissionDenied

        # The user must have the permissions we want
        if self.perms and not self.request.user.has_perms(self.perms):
            raise PermissionDenied

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kw):
        self.load_objects()
        res = self.check_permissions()
        if res is not None: return res
        return super(UserMixin, self).dispatch(request, *args, **kw)
