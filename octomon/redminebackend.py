import requests
from requests.auth import HTTPBasicAuth
from django.contrib.auth.models import User
from django.conf import settings

# REDMINE_AUTH_URL should be something like
# https://labs.truelite.it/users/current.json

class RedmineAuthBackend(object):
    def authenticate(self, username=None, password=None):
        URL = getattr(settings, "REDMINE_AUTH_URL", None)
        if not URL:
            return None

        try:
            user_info = requests.get(URL,
                                     auth=HTTPBasicAuth(username, password))
        except:
            return None

        if not user_info.ok:
            return None

        user_info = user_info.json()['user']
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            user = User.objects.create(
                username=username,
                first_name=user_info.get("firstname"),
                last_name=user_info.get("lastname"),
                email=user_info.get("mail"),
                is_active=True
                )
            user.set_password(password)
            user.save()
        else:
             user.set_password(password)
             user.save()
        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
