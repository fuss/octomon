function from_timestamp_to_string(ts, format) {
    return $.datepicker.formatDate(format, new Date(parseInt(ts)));
}

$(function() {
    // Make sure to add global 'data' variable in the template where you
    // include this js script, otherwise graph won't plot.
    // data = [{{data}}];
    var options = {
        xaxis: {
            mode: "time",
            timeformat: "%Y-%m-%d",
        },
        series: {
            lines: {
                show: true,
            },
            label: "Computer usage",
        },
        legend: {
            show: true,
        },
        selection: {
            mode: "x"
        },
        grid: {
            hoverable: true,
        },
    };

    var placeholder = $('#computer-usage-graph');
    var zoom_from = $('#zoom-from');
    var zoom_to = $('#zoom-to');
    var zoom = $('#zoom-to-selection');
    var reset_zoom = $('#reset-zoom');
    var initial_from = $('#initial-from');
    var initial_to = $('#initial-to');
    var zoom_from_ts, zoom_to_ts;

    zoom_from.datepicker({
        dateFormat: "yy-mm-dd",
        changeYear: true,
    });
    zoom_to.datepicker({
        dateFormat: "yy-mm-dd",
        changeYear: true,
    });

    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
            border: '1px solid #fdd',
            padding: '2px',
            'background-color': '#fee',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

    placeholder.bind("plotselected", function (event, ranges) {
        zoom_from.datepicker("setDate", from_timestamp_to_string(ranges.xaxis.from, 'yy-mm-dd'));
        zoom_to.datepicker("setDate", from_timestamp_to_string(ranges.xaxis.to, 'yy-mm-dd'));
        zoom_from_ts = ranges.xaxis.from;
        zoom_to_ts = ranges.xaxis.to;
    });

    var previousPoint = null;

    placeholder.bind("plothover", function (event, pos, item) {
        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;
                $("#tooltip").remove();
                var x = item.datapoint[0].toFixed(0),
                    y = item.datapoint[1].toFixed(0),
                    day = from_timestamp_to_string(x, 'yy-mm-dd');
                showTooltip(item.pageX, item.pageY, "" + day + " = " + y);
            }
        }
        else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });

    zoom.click(function() {
        if (zoom_from.val() && zoom_to.val()) {
            // get the y-max for the selected x range
            // Found here: http://imcol.in/2012/06/jquery-flot-selection-autoscale/
            var ymax;
            var plotdata = plot.getData();
            $.each(plotdata, function (e, val) {
                $.each(val.data, function (e1, val1) {
                    if ((val1[0] >= zoom_from_ts) && (val1[0] <= zoom_to_ts)) {
                        if (ymax == null || val1[1] > ymax) ymax = val1[1];
                    }
                })
            });

            plot = $.plot(placeholder, data, $.extend(true, {}, options, {
                xaxis: {
                    // Found here: http://stackoverflow.com/a/2407074
                    min: +(new Date(zoom_from.val())),
                    max: +(new Date(zoom_to.val()))
                },
                yaxis: {
                    min: 0, max: ymax * 1.1
                }
            }));
        }
    });

    reset_zoom.click(function() {
        zoom_from_ts = initial_from.val();
        zoom_to_ts = initial_to.val();
        zoom_from.datepicker("setDate", from_timestamp_to_string(initial_from.val(), 'yy-mm-dd'));
        zoom_to.datepicker("setDate", from_timestamp_to_string(initial_to.val(), 'yy-mm-dd'));
        zoom.click();
    });

    var plot = $.plot(placeholder, data, options);
    var axes = plot.getAxes();
    // Save min and max to be used by reset zoom button
    initial_from.val(axes.xaxis.min);
    initial_to.val(axes.xaxis.max);
    // Set from and to datepickers to initial values (min and max)
    zoom_from.datepicker("setDate", from_timestamp_to_string(initial_from.val(), 'yy-mm-dd'));
    zoom_to.datepicker("setDate", from_timestamp_to_string(initial_to.val(), 'yy-mm-dd'));
    // Set min and max date for the two datepickers, to avoid selection of dates outside graph
    $('.zoom-pickers').datepicker("option", "minDate", from_timestamp_to_string(axes.xaxis.min, 'yy-mm-dd'));
    $('.zoom-pickers').datepicker("option", "maxDate", from_timestamp_to_string(axes.xaxis.max, 'yy-mm-dd'));
});
