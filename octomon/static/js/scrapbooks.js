$(function() {
  // ScrapBook table
  var scrapbookTable = $("#scrapbooktable").dataTable({
    "aaSorting": [[ 0, "desc" ]],
    "sDom": "liprt",
  });
  // ScrapBook datepickers
  var $sc_sd = $("#scrapbook_start_date").datepicker({
    changeYear: true,
    onSelect: function(date) {
      scrapbookTable.fnDraw();
    },
    dateFormat: "yy-mm-dd",
  });
  var $sc_ed = $("#scrapbook_end_date").datepicker({
    changeYear: true,
    onSelect: function(date) {
      scrapbookTable.fnDraw();
    },
    dateFormat: "yy-mm-dd",
  });
  // ScrapBook datepickers' reset
  $('#scrapbook_reset_dates').click(function() {
    $sc_sd.attr('value', '');
    $.datepicker._clearDate($sc_sd);
    $sc_ed.attr('value', '');
    $.datepicker._clearDate($sc_ed);
  });
});
