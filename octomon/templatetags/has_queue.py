from django import template
from django.utils import http
from backend import Queue
from django.conf import settings

inbound_queue = getattr(settings, "INBOUND_QUEUE", "inbound")

register = template.Library()

@register.filter
def has_queue(val):
    queue = Queue(inbound_queue)
    return not queue.empty()
