from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from . import views as v

urlpatterns = patterns('',
    url(r'^$', 'octomon.views.home', name="home"),
    url(r'^login/$', 'octomon.views.octomon_login', name="login"),
    url(r'^logout/$', 'octomon.views.octomon_logout', name="logout"),
    url(r'^locationmap/$', v.LocationMap.as_view(), name="locationmap"),
    url(r'^user/(?P<id>[0-9]+)$', 'octomon.views.review_user', name="review_user"),
    url(r'^schools/', include('schools.urls')),
    url(r'^hosts/', include('hosts.urls')),
    url(r'^upload/', include('upload.urls')),
    url(r'^problems/', include('problems.urls')),
    url(r'^statistics/', include('stats.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^admin/', include(admin.site.urls)),
)
