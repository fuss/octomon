# coding: utf-8
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from django.views.generic import ListView
import schools.models as smodels
from .mixins import UserMixin

def octomon_login(request):
    username = password = ''
    _next = request.GET.get("next", None)
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                messages.info(request, _('Welcome, %s!') % (user.get_full_name() or username))
                if _next:
                    return redirect(_next)
                else:
                    return redirect('home')
            else:
                messages.error(request, _('Account %s is disabled' % username))
        else:
            messages.error(request,
                _("The credentials you supplied were not correct or did not grant access to this resource."))
    return render(request, 'login.html')


@login_required
def octomon_logout(request):
    logout(request)
    return redirect('home')


@login_required
def home(request):
    return redirect('locationmap')


class LocationMap(UserMixin, ListView):
    model = smodels.School
    template_name = "locationmap.html"
    context_object_name = "schools"
    def get_queryset(self):
        qs = super(LocationMap, self).get_queryset()
        if not self.request.user.is_superuser:
            qs = self.request.user.schools.all()
        return qs

@login_required
def review_user(request, id):
    user = get_object_or_404(User, pk=int(id))
    return render(request, 'review_user.html', {
        'reviewed_user': user,
    })
