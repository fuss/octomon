from datetime import datetime
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Table, TableStyle
from reportlab.platypus import Preformatted
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch, cm
from reportlab.lib import colors
from reportlab.lib.utils import ImageReader
from django.utils.translation import ugettext as _
import os.path

PAGE_HEIGHT=defaultPageSize[1]; PAGE_WIDTH=defaultPageSize[0]
styles = getSampleStyleSheet()

lm = 20

def pre(txt):
	return Preformatted(txt, styles['Code'])

class PdfReport:
    def __init__(self, school):
        self.school = school
        self.title = _("Report for: %(school)s - generated on %(date)s") % dict(school=self.school.name, date=datetime.now().strftime("%Y-%m-%d %H:%M"))
        self.background = self._get_background()
        self.logo = self._get_logo()

    def _get_logo(self):
        image = ImageReader(os.path.join(os.path.dirname(__file__), 'logo-80x80.png'))
        (width, heigth) = image.getSize()
        return ((width, heigth), image)

    def _get_background(self):
        image = ImageReader(os.path.join(os.path.dirname(__file__), 'logo.png'))
        (width, heigth) = image.getSize()
        return ((width, heigth), image)

    def Page(self,canvas, doc):
        canvas.saveState()
        ((width, height), image) = self.background
        canvas.drawImage(image, 2*inch, 3.75*inch, width, height)
        ((width, height), image) = self.logo
        canvas.drawImage(image, PAGE_WIDTH-110, PAGE_HEIGHT-100, width, height)
        canvas.setFont('Times-Roman',9)
        canvas.drawString(lm, 0.75 * inch, _("Page %(page)d - %(title)s") % dict(page=doc.page, title=self.title))
        canvas.restoreState()

    def saveToPdf(self, output_file):
        self.output_file = output_file
        doc = SimpleDocTemplate(self.output_file)

        Lines = []

        p = Paragraph(_("School: %s") % self.school.name, styles['h1'])
        Lines.append(p)

        p = Paragraph(_("Details"), styles["h2"])
        Lines.append(p)

        if self.school.area:
            p = Paragraph(_("Area: %s") % self.school.area.name, styles['Normal'])
            Lines.append(p)

        p = Paragraph(_("Address: %s") % self.school.address, styles['Normal'])
        Lines.append(p)

        p = Paragraph(_("Phone: %s") % self.school.phone_number, styles['Normal'])
        Lines.append(p)

        p = Paragraph(_("Email: %s") % self.school.email_address, styles['Normal'])
        Lines.append(p)

        p = Paragraph(_("Total computers: %d") % self.school.computers.all().count(), styles['Normal'])
        Lines.append(p)

        if len(self.school.managed.all()) > 0:
            line = _("Managed by: ")
            for x in self.school.managed.all():
                this = "%s (%s)" % (x.get_full_name() or x.username, x.email)
                line = "%s %s" % (line, this)
            p = Paragraph(line, styles['Normal'])
            Lines.append(p)

        p = Paragraph(_("Computers"), styles["h2"])
        Lines.append(p)
        line = "N   | %-25s | %-19s | %s" % (_("HOSTNAME"),
                                             _("LAST BOOT"),
                                             _("N. BOOT"))
        Lines.append(pre(line))
        for i, host in enumerate(self.school.computers.filter(dismissed=False)):
            line = "%-3d | %-25s | %19s | %s" % (i+1, host.hostname[:25], host.last_used.strftime("%Y-%m-%d %H%M"), host.usage_count)
            Lines.append(pre(line))

        doc.build(Lines, onFirstPage=self.Page, onLaterPages=self.Page)


class ActivityReport(PdfReport):
    def __init__(self, school):
        PdfReport.__init__(self, school)
        self.title = _("Activity for: %(school)s - generated on %(date)s") % dict(school=self.school.name, date=datetime.now().strftime("%Y-%m-%d %H:%M"))

    def saveToPdf(self, output_file, request):
        self.output_file = output_file
        doc = SimpleDocTemplate(self.output_file)
        Lines = []

        startDate = request.POST.get("pdf_report_start_date", None)
        endDate = request.POST.get("pdf_report_end_date", None)

        p = Paragraph(_("School: %s") % self.school.name, styles['h1'])
        Lines.append(p)

        p = Paragraph(_("Activity report from %(start_date)s to %(end_date)s") % dict(start_date=startDate, end_date=endDate), styles['Normal'])
        Lines.append(p)

        # table for activities
        data = [(x.timestamp.strftime("%Y-%m-%d\n%H:%M"),
                 x.end_timestamp.strftime("%Y-%m-%d\n%H:%M"),
                 x.user.username,
                 Paragraph(x.description,
                           styles['Normal'])) for x in self.school.activity_log(None, None, startDate, endDate)]
        data.insert(0, (_("State Date"), _("End date"), _("User"), _("Description")))

        s = Spacer(0.1*inch, 0.1*inch)
        Lines.append(s)

        p = Paragraph(_("Total activities: %d") % len(data), styles['Normal'])
        Lines.append(p)

        s = Spacer(0.5*inch, 0.5*inch)
        Lines.append(s)

        GRID_STYLE = TableStyle(
            [('GRID', (0,0), (-1,-1), 0.25, colors.black),
             ('ALIGN', (0,0), (-1,-1), 'LEFT'),
             ('VALIGN', (0,0), (-1,-1), "TOP")])


        t = Table( data, colWidths=[2.5*cm,2.5*cm, 2*cm, 13*cm])
        t.setStyle(GRID_STYLE)
        Lines.append(t)
        doc.build(Lines, onFirstPage=self.Page, onLaterPages=self.Page)
