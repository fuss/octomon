# coding: utf-8
from django.contrib import admin
from problems import models as pmodels

admin.site.register(pmodels.Alarm)
admin.site.register(pmodels.AlarmDefinition)
admin.site.register(pmodels.AlarmDefinitionRun)
admin.site.register(pmodels.Ticket)
admin.site.register(pmodels.TicketNote)
