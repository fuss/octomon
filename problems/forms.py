# coding: utf-8
from django import forms
from django.forms.models import modelformset_factory
import problems.models as pmodels


AlarmFormSet = modelformset_factory(
        pmodels.Alarm,
        fields=('ack',),
        extra=0,
    )

class OpenTicketForm(forms.ModelForm):
    class Meta:
        model = pmodels.Ticket
        fields = ('description',)
        widgets = {
            'description': forms.Textarea(attrs={'cols': 50, 'rows': 7}),
        }
