# coding: utf-8
from django.core.management.base import BaseCommand
from schools import models as smodels
from problems import models as pmodels
from hosts import models as hmodels
from datetime import datetime, timedelta


def is_affected(c, definition):
    alarm = False
    description = ""

    exec(definition.code)

    return alarm, description


class Command(BaseCommand):
    help = "Runs the alarms"

    def debug(self, *args):
        if self.DEBUG:
            args = [str(x) for x in args]
            print " ".join(args)

    def add_arguments(self, parser):
        parser.add_argument("--dry-run", action="store_true", dest="dry_run", default=False)
        parser.add_argument("--debug", action="store_true", dest="debug", default=False)

    def handle(self, *args, **options):
        self.DEBUG = options["debug"]
        self.DRY_RUN = options["dry_run"]

        for school in smodels.School.objects.filter(dismissed=False):
            self.debug("\nPROCESSING SCHOOL", school)
            for definition in pmodels.AlarmDefinition.objects.filter(active=True):
                try:
                    self.debug("Checking if this alarm has already been checkd for this timestap")
                    pmodels.AlarmDefinitionRun.objects.get(
                        school=school,
                        definition=definition,
                        school_timestamp=school.last_timestamp
                    )
                    self.debug("Yes, skipping...")
                    continue
                except:
                    pass
                self.debug("Checking if someone is affected of", definition)
                for host in hmodels.Computer.objects.filter(school=school):
                    try:
                        alarm, description = is_affected(host, definition)
                    except Exception as e:
                        self.debug(host, definition, "raised", e)
                        continue
                    if alarm:
                        self.debug(host, "is affected", alarm, description)
                        if not self.DRY_RUN:
                            try:
                                pmodels.Alarm.objects.create(
                                    host=host,
                                    definition=definition,
                                    description=description
                                    #timestamp=school.last_timestamp,
                                )
                            except:
                                # already exists, skip
                                pass
                if not self.DRY_RUN:
                    self.debug("Compiling an AlarmDefinitionRun object")
                    pmodels.AlarmDefinitionRun.objects.create(
                        school=school,
                        definition=definition,
                        school_timestamp=school.last_timestamp
                    )
