# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('schools', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Alarm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=200)),
                ('timestamp', models.DateTimeField(default=datetime.datetime.now, null=True, blank=True)),
                ('ack', models.BooleanField(default=False)),
                ('ack_when', models.DateTimeField(default=None, null=True, blank=True)),
                ('ack_by', models.ForeignKey(related_name='acked_alarms', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'alarms',
            },
        ),
        migrations.CreateModel(
            name='AlarmDefinition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=200)),
                ('code', models.CharField(max_length=2000)),
                ('active', models.BooleanField(default=False)),
                ('local', models.BooleanField(default=True)),
                ('user', models.ForeignKey(related_name='alarm_definitions', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'alarm_definition',
            },
        ),
        migrations.CreateModel(
            name='AlarmDefinitionRun',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('school_timestamp', models.FloatField(default=0, null=True, blank=True)),
                ('definition', models.ForeignKey(related_name='runs', to='problems.AlarmDefinition')),
                ('school', models.ForeignKey(related_name='alarm_definition_runs', to='schools.School')),
            ],
            options={
                'db_table': 'alarm_definition_run',
            },
        ),
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('opened', models.DateTimeField(null=True, blank=True)),
                ('ticket_for', models.CharField(max_length=200, blank=True)),
                ('object_id', models.IntegerField(null=True, blank=True)),
                ('description', models.CharField(max_length=2000, verbose_name='Problem description')),
                ('closed', models.DateTimeField(default=None, null=True, blank=True)),
                ('closed_by', models.ForeignKey(related_name='tickets_closed', to=settings.AUTH_USER_MODEL)),
                ('opened_by', models.ForeignKey(related_name='tickets_opened', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-closed', '-opened'),
                'db_table': 'ticket',
            },
        ),
        migrations.CreateModel(
            name='TicketNote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(null=True, blank=True)),
                ('content', models.CharField(max_length=2000, blank=True)),
                ('ticket', models.ForeignKey(related_name='notes', blank=True, to='problems.Ticket', null=True)),
                ('user', models.ForeignKey(related_name='ticket_notes', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'ticket_note',
            },
        ),
        migrations.AddField(
            model_name='alarm',
            name='definition',
            field=models.ForeignKey(related_name='alarms', blank=True, to='problems.AlarmDefinition'),
        ),
        migrations.AddField(
            model_name='alarm',
            name='host',
            field=models.ForeignKey(related_name='alarms', to='hosts.Computer'),
        ),
    ]
