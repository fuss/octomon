# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('problems', '0002_auto_20181103_2309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alarmdefinition',
            name='code',
            field=models.TextField(help_text='c is a Computer object'),
        ),
    ]
