# coding: utf-8
from datetime import datetime
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Alarm(models.Model):
    host = models.ForeignKey('hosts.Computer', related_name='alarms')
    definition = models.ForeignKey('AlarmDefinition', related_name='alarms', blank=True)
    description = models.CharField(max_length=200)
    timestamp = models.DateTimeField(default=datetime.now, null=True, blank=True)
    ack = models.BooleanField(default=False)
    ack_by = models.ForeignKey(User, related_name='acked_alarms', null=True, blank=True)
    ack_when = models.DateTimeField(default=None, null=True, blank=True)

    def __str__(self):
        return "%s %s %s" %(self.description, _("on"), self.host)

    class Meta:
        db_table = 'alarms'


@python_2_unicode_compatible
class AlarmDefinition(models.Model):
    user = models.ForeignKey(User, related_name='alarm_definitions')
    description = models.CharField(max_length=200)
    code = models.TextField(help_text=_("c is a Computer object"))
    active = models.BooleanField(default=False)
    local = models.BooleanField(default=True)

    def __str__(self):
        return self.description

    class Meta:
        db_table = 'alarm_definition'


@python_2_unicode_compatible
class AlarmDefinitionRun(models.Model):
    school = models.ForeignKey('schools.School', related_name='alarm_definition_runs')
    definition = models.ForeignKey('AlarmDefinition', related_name='runs')
    school_timestamp = models.FloatField(default=0, null=True, blank=True)

    def __str__(self):
        return "%s %s %s" %(self.definition, _("execution on"), self.school)

    class Meta:
        db_table = 'alarm_definition_run'


@python_2_unicode_compatible
class Ticket(models.Model):
    opened = models.DateTimeField(null=True, blank=True)
    opened_by = models.ForeignKey(User, related_name='tickets_opened')
    ticket_for = models.CharField(max_length=200, blank=True)
    object_id = models.IntegerField(null=True, blank=True)
    description = models.CharField(max_length=2000,
        verbose_name=_('Problem description'))
    closed = models.DateTimeField(default=None, null=True, blank=True)
    closed_by = models.ForeignKey(User, related_name='tickets_closed', null=True, blank=True)

    def __str__(self):
        return self.ticket_for

    class Meta:
        db_table = 'ticket'
        ordering = ('-closed', '-opened', )

    def get_duration(self):
        if self.closed:
            return self.closed - self.opened
        else:
            return 0

    @property
    def is_open(self):
        return False if self.closed else True

    @property
    def is_closed(self):
        return True if self.closed else False


@python_2_unicode_compatible
class TicketNote(models.Model):
    ticket = models.ForeignKey('Ticket', related_name='notes', null=True, blank=True)
    user = models.ForeignKey(User, related_name='ticket_notes')
    timestamp = models.DateTimeField(null=True, blank=True)
    content = models.CharField(max_length=2000, blank=True)

    def __str__(self):
        return "%s %s" %(self.ticket, _("comment"))

    class Meta:
        db_table = 'ticket_note'
