$(function() {
    var alarmTable = $("#alarmtable").dataTable({
        "bLengthChange": false,
        // Remove search box http://datatables.net/ref#sDom
        "sDom": "lrtip",
    });
    var $sd = $("#start_date").datepicker({
        changeYear: true,
        onSelect: function(date) {
            alarmTable.fnDraw();
        },
        dateFormat: "yy-mm-dd",
    });
    var $ed = $("#end_date").datepicker({
        changeYear: true,
        onSelect: function(date) {
            alarmTable.fnDraw();
        },
        dateFormat: "yy-mm-dd",
    });
    $('#reset_dates').click(function() {
        $sd.attr('value', '');
        $.datepicker._clearDate($sd);
        $ed.attr('value', '');
        $.datepicker._clearDate($ed);
    });
});

// Some ugly javascript to manage the filtering in the third column for range of dates
$.fn.dataTableExt.afnFiltering.push(function( oSettings, aData, iDataIndex ) {
    if (oSettings.sTableId == 'alarmtable') {
        var start_date = $('#start_date').datepicker("getDate");
        var end_date = $('#end_date').datepicker("getDate");
        // The column to filter for in alarm table is the third
        var date = aData[2];
    }
    if (oSettings.sTableId == 'activitytable') {
        var start_date = $('#activity_start_date').datepicker("getDate");
        var end_date = $('#activity_end_date').datepicker("getDate");
        // The column to filter for in activity table is the first
        var date = aData[0];
    }
    if (oSettings.sTableId == 'scrapbooktable') {
        var start_date = $('#scrapbook_start_date').datepicker("getDate");
        var end_date = $('#scrapbook_end_date').datepicker("getDate");
        // The column to filter for in activity table is the first
        var date = aData[0];
    }
    // Some checking to avoid errors when converting from date to strings...
    // Bad code that works, for now. Feel free to make it better!
    if (start_date) {
        var year = start_date.getFullYear();
        var month = start_date.getMonth() + 1;
        if (month < 10) month = '0' + month;
        var day = start_date.getDate();
        if (day < 10) day = '0' + day;
        str_start_date = "" + year + month + day;
    } else {
        var str_start_date = "";
    }
    if (end_date) {
        var year = end_date.getFullYear();
        var month = end_date.getMonth() + 1;
        if (month < 10) month = '0' + month;
        var day = end_date.getDate();
        if (day < 10) day = '0' + day;
        str_end_date = "" + year + month + day;
    } else {
        var str_end_date = "";
    }

    // remove the time stamp out of my date
    // 2010-04-11 20:48:22 -> 2010-04-11
    date = date.substring(0,10);
    // remove the "-" characters
    // 2010-04-11 -> 20100411
    date = date.substring(0,4) + date.substring(5,7) + date.substring(8,10);

    // run through cases
    if ( str_start_date == "" && date <= str_end_date){
        return true;
    }
    else if ( str_start_date =="" && date <= str_end_date ){
        return true;
    }
    else if ( str_start_date <= date && "" == str_end_date ){
        return true;
    }
    else if ( str_start_date <= date && date <= str_end_date ){
        return true;
    }
    // all failed
    return false;
});
