from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^alarms/non_ack', 'problems.views.list_non_acked_alarms', name="non_acked_alarms"),
    url(r'^alarm/do_ack/(?P<id>[0-9]+)$', 'problems.views.ack_alarm', name="id_do_ack_alarm"),
    url(r'^alarms/ack/$', 'problems.views.ack_alarms', name="ack_alarms"),
    url(r'^tickets/open$', 'problems.views.list_open_tickets', name="list_open_tickets"),
    url(r'^tickets/closed$', 'problems.views.list_closed_tickets', name="list_closed_tickets"),
    url(r'^tickets/new/computer/(?P<id>[0-9]+)$', 'problems.views.open_ticket_for_computer', name="open_ticket_for_computer"),
    url(r'^tickets/new/other/(?P<id>[0-9]+)$', 'problems.views.open_ticket_for_other', name="open_ticket_for_other"),
    url(r'^tickets/close/(?P<id>[0-9]+)$', 'problems.views.close_ticket', name="close_ticket"),
)
