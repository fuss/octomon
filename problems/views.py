# coding: utf-8
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from datetime import datetime
import schools.models as smodels
import hosts.models as hmodels
import problems.models as pmodels
import problems.forms as pforms
from django.forms.models import model_to_dict


def annotate_tickets(tickets):
    """
    Auxiliary function that converts a Ticket model object in a dict,
    and adds various data to it, useful for the views that list open
    and closed tickets.
    """
    annotated_tickets = []
    for ticket in tickets:
        this_ticket = model_to_dict(ticket)
        if ticket.opened_by:
            this_ticket['opened_by'] = get_object_or_404(User, pk=ticket.opened_by_id)
        if ticket.is_closed:
            this_ticket['closed_by'] = get_object_or_404(User, pk=ticket.closed_by_id)
            this_ticket['duration'] = ticket.get_duration()
        if ticket.ticket_for == 'Computer':
            computer = get_object_or_404(hmodels.Computer, pk=int(ticket.object_id))
            this_ticket['object_instance'] = computer
            this_ticket['object_name'] = computer.hostname
            this_ticket['school'] = computer.school
        elif ticket.ticket_for == 'Other hardware':
            other_hardware = get_object_or_404(smodels.OtherHardware, pk=int(ticket.object_id))
            this_ticket['object_instance'] = other_hardware
            this_ticket['object_name'] = other_hardware.kind.name
            this_ticket['school'] = other_hardware.school
        annotated_tickets.append(this_ticket)
    return annotated_tickets


@login_required
def ack_alarms(request):
    alarmformset = pforms.AlarmFormSet(request.POST)
    school = alarmformset.forms[0].instance.host.school
    if not request.user.has_perm('problems.change_alarm'):
        messages.error(request, _("You don't have the permission to modify alarms"))
        return HttpResponseRedirect(reverse('school_detail', args=[school.id,]))
    if alarmformset.is_valid():
        alarms = alarmformset.save(commit=False)
        for alarm in alarms:
            if alarm.ack:
                alarm.ack_by = request.user
                alarm.ack_when = datetime.now()
                alarm.save()
    else:
        pass #print alarmformset.errors
    return HttpResponseRedirect(reverse('school_detail', args=[school.id,]))


@login_required
def list_open_tickets(request):
    tickets = pmodels.Ticket.objects.filter(closed_by__isnull=True)
    open_tickets = list(annotate_tickets(tickets))
    if not request.user.is_superuser:
        schools = request.user.schools.all()
        open_tickets = [x for x in open_tickets if x['school'] in schools]
    return render(request, 'problems/list_open_tickets.html', {
        'open_tickets': open_tickets,
    })


@login_required
def list_closed_tickets(request):
    tickets = pmodels.Ticket.objects.filter(closed_by__isnull=False)
    closed_tickets = list(annotate_tickets(tickets))
    if not request.user.is_superuser:
        schools = request.user.schools.all()
        closed_tickets = [x for x in closed_tickets if x['school'] in schools]
    return render(request, 'problems/list_closed_tickets.html', {
        'closed_tickets': closed_tickets,
    })


@login_required
def open_ticket_for_computer(request, id):
    if not request.user.has_perm('problems.add_ticket'):
        messages.error(request, _("You don't have the permission to create tickets"))
        return HttpResponseRedirect(reverse('host', args=[id,]))
    if request.method == 'POST':
        form = pforms.OpenTicketForm(request.POST)
        if form.is_valid():
            ticket = form.save(commit=False)
            ticket.opened = datetime.now()
            ticket.opened_by = request.user
            ticket.ticket_for = 'Computer'
            ticket.object_id = int(id)
            ticket.save()
            return redirect('list_open_tickets')
        else:
            messages.error(request, _('Error, ticket not created'))
    else:
        form = pforms.OpenTicketForm()
    return render(request, 'problems/open_ticket.html', {
        'form': form,
    })


@login_required
def open_ticket_for_other(request, id):
    if not request.user.has_perm('problems.add_ticket'):
        messages.error(request, _("You don't have the permission to create tickets"))
        other_hardware = get_object_or_404(smodels.OtherHardware, pk=int(id))
        return HttpResponseRedirect(reverse('school_detail', args=[other_hardware.school.id,]))
    if request.method == 'POST':
        form = pforms.OpenTicketForm(request.POST)
        if form.is_valid():
            ticket = form.save(commit=False)
            ticket.opened = datetime.now()
            ticket.opened_by = request.user
            ticket.ticket_for = 'Other hardware'
            ticket.object_id = int(id)
            ticket.save()
            return redirect('list_open_tickets')
        else:
            messages.error(request, _('Error, ticket not created'))
    else:
        form = pforms.OpenTicketForm()
    return render(request, 'problems/open_ticket.html', {
        'form': form,
    })


@login_required
def close_ticket(request, id):
    if not request.user.has_perm('problems.change_ticket'):
        messages.error(request, _("You don't have the permission to modify tickets"))
        return redirect('list_closed_tickets')
    ticket = get_object_or_404(pmodels.Ticket, pk=int(id))
    ticket.closed = datetime.now()
    ticket.closed_by = request.user
    ticket.save()
    messages.info(request, _('Ticket closed successfully'))
    return redirect('list_closed_tickets')


@login_required
def list_non_acked_alarms(request):
    schools = request.user.schools.all()
    if request.user.is_superuser:
        schools = smodels.School.objects.all()
    alarms = pmodels.Alarm.objects.filter(host__school__in=schools, ack=False)
    return render(request, "problems/non_acked_alarms.html", {"alarms": alarms})


@login_required
def ack_alarm(request, id):
    if not request.user.has_perm('problems.change_alarm'):
        messages.error(request, _("You don't have the permission to modify alarms"))
        return HttpResponseRedirect(reverse('non_acked_alarms'))
    alarm = get_object_or_404(pmodels.Alarm, pk=int(id))
    if alarm.host.school not in request.user.schools.all() and not user.is_superuser:
        messages.error(request, _("You cannot operate on this school"))
        return HttpResponseRedirect(reverse('non_acked_alarms'))
    if alarm.ack:
        messages.error(request, _("This alarm is already hushed up"))
        return HttpResponseRedirect(reverse('non_acked_alarms'))
    alarm.ack = True
    alarm.ack_by = request.user
    alarm.ack_when = datetime.now()
    alarm.save()
    messages.success(request, _("Alarm %(desc)s on %(host)s has been hushed up") % {
        "desc": alarm.description,
        "host": alarm.host
    })
    return redirect("non_acked_alarms")
