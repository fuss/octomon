# coding: utf-8
from django.contrib import admin
import schools.models as smodels


class SchoolAdmin(admin.ModelAdmin):
    list_display = ['name','address','area', 'dismissed']
    list_filter = ['dismissed', 'area']
    def get_queryset(self, request):
        return smodels.School.all_objects.all()

class SchoolKeyAdmin(admin.ModelAdmin):
    list_display = ['fingerprint', 'school']
    search_fields = ['fingerprint']

class SchoolKeyUsageAdmin(admin.ModelAdmin):
    list_display = ['keyfingerprint',
                    'school',
                    'timestamp',
                    'first_data_timestamp',
                    'last_data_timestamp']
    list_filter = ['timestamp', 'school']
    search_fields = ['key__fingerprint',
                     'school__name']
    def keyfingerprint(self, obj):
        return obj.key.fingerprint

admin.site.register(smodels.Area)
admin.site.register(smodels.School, SchoolAdmin)
admin.site.register(smodels.SchoolProp)
admin.site.register(smodels.SchoolKey, SchoolKeyAdmin)
admin.site.register(smodels.SchoolKeyUsage, SchoolKeyUsageAdmin)
admin.site.register(smodels.Activity)
admin.site.register(smodels.SchoolScrapBook)
admin.site.register(smodels.OtherHardware)
admin.site.register(smodels.OtherHardwareKind)
