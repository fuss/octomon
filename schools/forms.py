# coding: utf-8
from django import forms
import schools.models as smodels



class ActivityForm(forms.ModelForm):
    class Meta:
        model = smodels.Activity
        fields = (
            'timestamp', 'end_timestamp', 'description'
        )
        widgets = {
            'description': forms.Textarea()
        }


class NewActivityForm(forms.ModelForm):
    class Meta:
        model = smodels.Activity
        fields = (
            'school', 'user', 'timestamp', 'end_timestamp', 'description'
        )
        widgets = {
            'description': forms.Textarea()
        }


class SchoolScrapBookForm(forms.ModelForm):
    class Meta:
        model = smodels.SchoolScrapBook
        fields = (
            'content', 'school',
        )
        widgets = {
            'content': forms.Textarea(attrs={'cols': 50, 'rows': 7}),
            'school': forms.HiddenInput(),
        }


class OtherHardwareForm(forms.ModelForm):
    class Meta:
        model = smodels.OtherHardware
        fields = (
            'kind', 'vendor', 'model',
            'serial_number', 'location', 'warranty_expire', 'dismissed',
        )
