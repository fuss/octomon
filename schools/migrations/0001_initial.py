# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Activity',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=20000)),
                ('timestamp', models.DateTimeField(default=datetime.datetime.now, null=True, verbose_name='Activity start date', blank=True)),
                ('end_timestamp', models.DateTimeField(default=datetime.datetime.now, null=True, verbose_name='Activity end date', blank=True)),
            ],
            options={
                'ordering': ('school', '-timestamp'),
                'db_table': 'activity',
                'verbose_name_plural': 'activities',
            },
        ),
        migrations.CreateModel(
            name='Area',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=50, blank=True)),
                ('name', models.CharField(max_length=150, blank=True)),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'area',
            },
        ),
        migrations.CreateModel(
            name='OtherHardware',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vendor', models.CharField(max_length=200, blank=True)),
                ('model', models.CharField(max_length=200, blank=True)),
                ('serial_number', models.CharField(max_length=200, blank=True)),
                ('warranty_expire', models.DateField(null=True, verbose_name='Warranty expires on', blank=True)),
                ('location', models.CharField(max_length=200, blank=True)),
                ('dismissed', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'other_hardware',
            },
        ),
        migrations.CreateModel(
            name='OtherHardwareKind',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, blank=True)),
                ('description', models.CharField(max_length=200, blank=True)),
            ],
            options={
                'db_table': 'other_hardware_kind',
            },
        ),
        migrations.CreateModel(
            name='School',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200, blank=True)),
                ('address', models.CharField(max_length=300, blank=True)),
                ('phone_number', models.CharField(max_length=200, blank=True)),
                ('email_address', models.CharField(max_length=200, blank=True)),
                ('lat', models.FloatField(null=True, verbose_name='Latitude', blank=True)),
                ('lon', models.FloatField(null=True, verbose_name='Longitude', blank=True)),
                ('last_timestamp', models.FloatField(default=0.0, null=True, blank=True)),
                ('area', models.ForeignKey(related_name='schools', verbose_name='Group', blank=True, to='schools.Area', null=True)),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'school',
            },
        ),
        migrations.CreateModel(
            name='SchoolKey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fingerprint', models.CharField(unique=True, max_length=200)),
                ('crypto_key', models.TextField(blank=True)),
                ('school', models.ForeignKey(related_name='keys', to='schools.School')),
            ],
            options={
                'ordering': ('school', 'fingerprint'),
                'db_table': 'school_key',
            },
        ),
        migrations.CreateModel(
            name='SchoolKeyUsage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(default=datetime.datetime.now, null=True, blank=True)),
                ('first_data_timestamp', models.FloatField(null=True, blank=True)),
                ('last_data_timestamp', models.FloatField(null=True, blank=True)),
                ('key', models.ForeignKey(related_name='usages', blank=True, to='schools.SchoolKey', null=True)),
                ('school', models.ForeignKey(to='schools.School')),
            ],
            options={
                'ordering': ('key', 'timestamp'),
                'db_table': 'school_key_usage',
            },
        ),
        migrations.CreateModel(
            name='SchoolProp',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('propname', models.CharField(max_length=200)),
                ('propvalue', models.CharField(max_length=200)),
                ('school', models.ForeignKey(related_name='props', to='schools.School')),
            ],
            options={
                'ordering': ('school', 'propname'),
                'db_table': 'school_props',
            },
        ),
        migrations.CreateModel(
            name='SchoolScrapBook',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.CharField(max_length=2000)),
                ('timestamp', models.DateTimeField(default=datetime.datetime.now, null=True, blank=True)),
                ('school', models.ForeignKey(related_name='scrapbooks', to='schools.School')),
                ('user', models.ForeignKey(related_name='school_scrapbooks', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('school', '-timestamp'),
                'db_table': 'scrap_book',
            },
        ),
        migrations.CreateModel(
            name='SchoolsManagers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('manager', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('school', models.ForeignKey(to='schools.School')),
            ],
            options={
                'verbose_name_plural': 'Schools Managers',
            },
        ),
        migrations.AddField(
            model_name='school',
            name='managed',
            field=models.ManyToManyField(related_name='schools', through='schools.SchoolsManagers', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='otherhardware',
            name='kind',
            field=models.ForeignKey(related_name='other_hardwares', verbose_name='Kind', blank=True, to='schools.OtherHardwareKind', null=True),
        ),
        migrations.AddField(
            model_name='otherhardware',
            name='school',
            field=models.ForeignKey(related_name='other_hardwares', verbose_name='School', to='schools.School'),
        ),
        migrations.AddField(
            model_name='activity',
            name='school',
            field=models.ForeignKey(related_name='activities', to='schools.School'),
        ),
        migrations.AddField(
            model_name='activity',
            name='user',
            field=models.ForeignKey(related_name='activities', to=settings.AUTH_USER_MODEL),
        ),
    ]
