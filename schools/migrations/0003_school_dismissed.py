# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schools', '0002_auto_20170310_1609'),
    ]

    operations = [
        migrations.AddField(
            model_name='school',
            name='dismissed',
            field=models.BooleanField(default=False),
        ),
    ]
