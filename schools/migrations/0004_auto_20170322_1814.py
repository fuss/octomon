# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schools', '0003_school_dismissed'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='schoolkey',
            options={'ordering': ('school', '-pk', 'fingerprint')},
        ),
    ]
