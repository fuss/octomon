# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schools', '0003_school_dismissed'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='schoolkey',
            options={'ordering': ('school', '-pk', 'fingerprint')},
        ),
        migrations.AddField(
            model_name='school',
            name='lasis_code',
            field=models.CharField(max_length=16, null=True, blank=True),
        ),
    ]
