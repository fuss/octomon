# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schools', '0004_auto_20170405_0956'),
    ]

    operations = [
        migrations.AlterField(
            model_name='school',
            name='lasis_code',
            field=models.CharField(max_length=16, null=True, verbose_name='LASIS School code', blank=True),
        ),
    ]
