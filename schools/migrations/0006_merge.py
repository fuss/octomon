# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('schools', '0004_auto_20170322_1814'),
        ('schools', '0005_auto_20170405_1004'),
    ]

    operations = [
    ]
