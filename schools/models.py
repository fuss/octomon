# coding: utf-8
from datetime import datetime
from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from stats.const import components_stats_to_be_kept


# this object is to create groups of school
# in italy they're called "Istituto Comprensivo"
# which manages more than one school, usually small ones.
class Area(models.Model):
    code = models.CharField(max_length=50, blank=True, verbose_name=_("code"))
    name = models.CharField(max_length=150, blank=True, verbose_name=_("name"))

    class Meta:
        db_table = 'area'
        ordering = ('name', )

    def __unicode__(self):
        return "%s (%s)" % (self.name, self.code)

class SchoolManager(models.Manager):
    def get_queryset(self):
        return super(SchoolManager, self).get_queryset().filter(dismissed=False)

class AllSchoolManager(models.Manager):
    def get_queryset(self):
        return super(AllSchoolManager, self).get_queryset()

# this object represent a LAN
# but having also other attributes regarding building locations
# and so, we call it a "school"
class School(models.Model):

    objects = SchoolManager()
    all_objects = AllSchoolManager()

    name = models.CharField(max_length=200, unique=True,
                            blank=False, verbose_name=_("School name"))
    area = models.ForeignKey("Area", related_name="schools",
                             null=True, blank=True, #cascade=False, notNull=False
        verbose_name=_('Group'))
    address = models.CharField(max_length=300, blank=True)
    phone_number = models.CharField(max_length=200, blank=True)
    # TODO: EmailField?
    email_address = models.CharField(max_length=200, blank=True)
    lat = models.FloatField(null=True, blank=True,
        verbose_name=_('Latitude'))
    lon = models.FloatField(null=True, blank=True,
        verbose_name=_('Longitude'))
    last_timestamp = models.FloatField(default=0.0, null=True, blank=True)
    managed = models.ManyToManyField(User, related_name='schools', through='SchoolsManagers')
    dismissed = models.BooleanField(default=False)
    lasis_code = models.CharField(max_length=16, null=True, blank=True,
                                  verbose_name=_("LASIS School code"))

    class Meta:
        db_table = 'school'
        ordering = ('name', )

    def get_absolute_url(self):
        return reverse('school_detail', kwargs={'pk': self.pk})

    def __unicode__(self):
        return "%s" % self.name

    def get_all_alarms(self):
        alarms = []
        for computer in self.computers.all():
            alarms.extend(computer.get_all_alarms())
        return alarms

    def get_acked_alarms(self):
        alarms = []
        for computer in self.computers.all():
            alarms.extend(computer.get_acked_alarms())
        return alarms

    def get_non_acked_alarms(self):
        alarms = []
        for computer in self.computers.all():
            alarms.extend(computer.get_non_acked_alarms())
        return alarms

    def get_last_state(self):
        if self.get_non_acked_alarms():
            return "PROBLEMS"
        else:
            return "OK"

    def get_last_timestamp(self):
        if self.last_timestamp:
            return datetime.fromtimestamp(self.last_timestamp)

    def has_server(self):
        server = self.computers.filter(hostname__istartswith=":SERVER:")
        return len(server) and server[0] or None

    def get_server(self):
        server = self.computers.filter(hostname__istartswith=":SERVER:")
        if len(server) == 1:
            return server[0]
        else:
            return None

    def get_computers(self):
        return self.computers.exclude(hostname__istartswith=":SERVER:")

    def get_active_computers(self):
        return self.computers.exclude(dismissed=True)

    def count_computers(self):
        computers = self.get_computers()
        return computers.count()

    def count_active_computers(self):
        computers = self.get_active_computers()
        return computers.count()

    def get_total_activity_hours(self):
        hours = 0.0
        for activity in self.activities.all():
            delta = activity.end_timestamp - activity.timestamp
            hours += (delta.seconds / 3600.0)
        return int(hours)

    def activity_log(self, first=None, limit=30, startDate=None, endDate=None):
        return Activity.objects.filter(school=self,
                                      timestamp__gte=startDate,
                                      timestamp__lt=endDate)

    def get_total_users(self):
        props = self.props.filter(propname='total_users')
        if len(props) > 0:
            return int(props[0].propvalue)
        else:
            return 0

    def get_other_hardware_all(self):
        return self.other_hardwares.all()

    def get_other_hardware_dismissed(self):
        return self.other_hardwares.filter(dismissed=True)

    def get_other_hardware_in_school(self):
        return self.other_hardwares.filter(dismissed=False)

    def get_daily_uses(self):
        data = {}
        for computer in self.get_computers():
            computer_daily_uses = computer.get_daily_uses()
            for key in computer_daily_uses:
                try:
                    data[key] += computer_daily_uses[key]
                except:
                    data[key] = computer_daily_uses[key]
        return data

    def count_daily_computers_used(self):
        data = {}
        for computer in self.get_computers():
            computer_days = computer.get_days_of_use()
            for key in computer_days:
                try:
                    data[key] += computer_days[key]
                except:
                    data[key] = computer_days[key]
        return data

    def get_components_stats(self):
        import re
        def _round_to_hundreds(val):
            """
            Rounds to the nearest hundreds
            """
            value = val
            print(value, type(value))
            if type(value) == type(""):
                if " " in value:
                    value = float(value.split(" ")[0])

            return int(round(value / 100.0) * 100)

        school_stats = {}
        for x in components_stats_to_be_kept:
            school_stats[x] = dict()
        for computer in self.computers.filter(dismissed__exact=False):
            for component in computer.get_last_components():
                if component.name in components_stats_to_be_kept:
                    name = component.name
                    if name == 'cpuspeed':
                        # Keep only numbers and decimal points. If there is one
                        # or more '.', split over '.' and keep the first part.
                        # Parse to int and round to hundreds
                        component.value = re.sub("[^0-9\.]", "", component.value)
                        if re.match('.', component.value):
                            component.value = component.value.split('.')[0]
                        try:
                            component.value = "%d" % _round_to_hundreds(int(component.value))
                        except:
                            pass
                    if name == 'cputype':
                        # Clean, and remove unnecessary whitespace
                        component.value = re.sub("^.*model name.*:\s", "", component.value)
                        component.value = re.sub("  +", " ", component.value)
                    if name == 'totalram':
                        # Round to hundreds
                        component.value = component.value
                    value = component.value.strip()
                    actual_count = int(school_stats.get(name).get(value, 0))
                    school_stats[name][value] = actual_count + 1
        return school_stats

class SchoolsManagers(models.Model):
    school = models.ForeignKey('School')
    manager = models.ForeignKey(User)

    class Meta:
        verbose_name_plural = 'Schools Managers'


class SchoolProp(models.Model):
    school = models.ForeignKey('School', related_name='props')
    propname = models.CharField(max_length=200)
    propvalue = models.CharField(max_length=200)

    class Meta:
        db_table = 'school_props'
        ordering = ('school', 'propname', )

    def __unicode__(self):
        return "%s - %s" % (self.school.name, self.propname)


class SchoolKey(models.Model):
    school = models.ForeignKey('School', related_name='keys')
    fingerprint = models.CharField(max_length=200, unique=True)
    crypto_key = models.TextField(blank=True)

    class Meta:
        db_table = 'school_key'
        ordering = ('school', '-pk', 'fingerprint', )

    # Using self.school (and School.__unicode__() return self.name) gives
    # DjangoUnicodeDecodeError, using self.school.name here fixes the problem
    def __unicode__(self):
        return "%s - %s" % (self.school.name, self.fingerprint)


class SchoolKeyUsage(models.Model):
    school = models.ForeignKey('School')
    key = models.ForeignKey('SchoolKey', related_name='usages', null=True, blank=True)
    timestamp = models.DateTimeField(default=datetime.now, null=True, blank=True)
    first_data_timestamp = models.FloatField(null=True, blank=True)
    last_data_timestamp = models.FloatField(null=True, blank=True)

    class Meta:
        db_table = 'school_key_usage'
        ordering = ('key', 'timestamp', )

    def get_first_timestamp(self):
        return datetime.fromtimestamp(self.first_data_timestamp)

    def get_last_timestamp(self):
        return datetime.fromtimestamp(self.last_data_timestamp)

    def print_timestamps(self):
        return "%s - (from %s to %s)" % (self.timestamp, self.get_first_timestamp(), self.get_last_timestamp())

    # Use explicit fields to avoid DjangoUnicodeDecodeError when leaving
    # models' __unicode__() methods do the job, just like in
    # SchoolKey.__unicode__()
    def __unicode__(self):
        return "%s - %s - %s" % (self.key.school.name, self.key.fingerprint, self.timestamp)


class Activity(models.Model):
    school = models.ForeignKey('School', related_name='activities')
    user = models.ForeignKey(User, related_name='activities')
    description = models.CharField(max_length=20000)
    timestamp = models.DateTimeField(default=datetime.now, null=True, blank=True,
            verbose_name=_('Activity start date'))
    end_timestamp = models.DateTimeField(default=datetime.now, null=True, blank=True,
            verbose_name=_('Activity end date'))

    class Meta:
        db_table = 'activity'
        ordering = ('school', '-timestamp', )
        verbose_name_plural = 'activities'

    def __unicode__(self):
        return "%s - %s" % (self.school.name, self.timestamp)


class SchoolScrapBook(models.Model):
    school = models.ForeignKey('School', related_name='scrapbooks')
    user = models.ForeignKey(User, related_name='school_scrapbooks')
    content = models.CharField(max_length=2000)
    timestamp = models.DateTimeField(default=datetime.now, null=True, blank=True)

    class Meta:
        db_table = 'scrap_book'
        ordering = ('school', '-timestamp', )

    def __unicode__(self):
        return "%s - %s" % (self.school.name, self.timestamp)


class OtherHardware(models.Model):
    school = models.ForeignKey('School', related_name='other_hardwares',
            verbose_name=_('School'))
    kind = models.ForeignKey('OtherHardwareKind', related_name='other_hardwares',
            null=True, blank=True, verbose_name=_('Kind'))
    vendor = models.CharField(max_length=200, blank=True, verbose_name=_("Vendor"))
    model = models.CharField(max_length=200, blank=True, verbose_name=_("Model"))
    serial_number = models.CharField(max_length=200, blank=True, verbose_name=_("Serial number"))
    warranty_expire = models.DateField(null=True, blank=True,
            verbose_name=_('Warranty expires on'))
    location = models.CharField(max_length=200, blank=True, verbose_name=_("Location"))
    # ./manage.py inspectdb gave a models.IntegerField(null=True, blank=True)
    # MySQL has a TINYINT(4). Django stores BooleanFields as TINYINT when the
    # DB doesn't have a BOOLEAN type (like MySQL). For now I use a BooleanField
    # with default=False, which seems plausible.
    created_by = models.ForeignKey(User, verbose_name=_("Created by"))
    dismissed = models.BooleanField(default=False)

    class Meta:
        db_table = 'other_hardware'

    def __unicode__(self):
        return "%s (%s)" % (
            self.kind.name or None,
            self.school.name or None,
        )


class OtherHardwareKind(models.Model):
    name = models.CharField(max_length=200, blank=True, verbose_name=_("name"))
    description = models.CharField(max_length=200, blank=True, verbose_name=_("description"))

    class Meta:
        db_table = 'other_hardware_kind'

    def __unicode__(self):
        return self.name
