from django.conf.urls import patterns, url
from . import views as v

urlpatterns = patterns('',
    url(r'^areas/$', v.AreaList.as_view(), name="areas"),
    url(r'^area/(?P<id>[0-9]+)?$', 'schools.views.area', name="area"),
    url(r'^list/$', v.SchoolList.as_view(), name="schools"),
    url(r'^detail/(?P<pk>[0-9]+)?$', v.SchoolDetail.as_view(), name="school_detail"),
    url(r'^create/$', v.SchoolCreate.as_view(), name="school_create"),
    url(r'^update/(?P<pk>[0-9]+)$', v.SchoolUpdate.as_view(), name="school_update"),
    url(r'^delete/(?P<id>[0-9]+)$', 'schools.views.delete_school', name="delete_school"),
    url(r'^pdf_report/(?P<id>[0-9]+)$', 'schools.views.pdf_report', name="pdf_report_school"),
    url(r'^pdf_activity_report/(?P<id>[0-9]+)$', 'schools.views.pdf_activity_report', name="pdf_activity_report_school"),
    url(r'^csv_activity_report/(?P<id>[0-9]+)$', 'schools.views.csv_activity_report', name="csv_activity_report_school"),
    # The id in add_activity url is the SCHOOL id (we want to add a new
    # activity to xyz school)
    url(r'^add_activity/(?P<id>[0-9]+)$', 'schools.views.add_activity', name="add_activity"),
    # The id in edit_activity url is the ACTIVITY id (we want to modify the xyz
    # activity instance)
    url(r'^edit_activity/(?P<id>[0-9]+)$', 'schools.views.edit_activity', name="edit_activity"),
    url(r'^drop_other_hardware/(?P<pk>[0-9]+)$', v.DropOtherHardware.as_view(), name='drop_other_hardware'),
    # The id in add_scrapbook url is the SCHOOL id
    url(r'^add_scrapbook/(?P<id>[0-9]+)$', 'schools.views.add_scrapbook', name="add_school_scrapbook"),
    url(r'^create_other_hardware/(?P<id>[0-9]+)$', 'schools.views.create_other_hardware', name="create_other_hardware"),
    url(r'^edit_other_hardware/(?P<hw>[0-9]+)$', 'schools.views.edit_other_hardware', name="edit_other_hardware"),
    url(r'^computers/dismissed/(?P<id>[0-9]+)?$', 'schools.views.computers_dismissed', name="computers_dismissed"),
    url(r'^get_schools_activities_json/(?P<id>[0-9]+)?$', 'schools.views.get_schools_activities_json', name="get_schools_activities_json"),
    url(r'^search_activities/$', 'schools.views.search_activities', name="search_activities"),
    url(r'^log/$', 'schools.views.school_log', name="school_log"),
)
