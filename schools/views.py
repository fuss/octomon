# coding: utf-8
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django import http
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
import schools.models as smodels
import problems.models as pmodels
import schools.forms as sforms
import problems.forms as pforms
import pdf
from csvreports import CSVReport
from io import BytesIO
import json
from django.db.models import Max
from django.views.generic import ListView, CreateView, DeleteView
from django.views.generic import UpdateView, DetailView
from octomon.mixins import UserMixin
import stats.models as stmodels
import datetime as dt

class AreaList(UserMixin, ListView):
    model = smodels.Area
    template_name = "schools/areas.html"

@login_required
def area(request, id):
    area = get_object_or_404(smodels.Area, pk=int(id))
    return render(request, 'schools/area.html', {
        'area': area,
    })

@login_required
def pdf_report(request, id):
    response = http.HttpResponse(content_type='application/pdf')
    school = get_object_or_404(smodels.School, pk=id)
    response['Content-Disposition'] = 'attachment; filename=global_report_school_%d.pdf' % school.id
    buffer = BytesIO()
    form = pdf.reports.PdfReport(school)
    form.saveToPdf(buffer)
    # Get the value of the BytesIO buffer and write it to the response.
    data = buffer.getvalue()
    buffer.close()
    response.write(data)
    return response

@login_required
def pdf_activity_report(request, id):
    response = http.HttpResponse(content_type='application/pdf')
    school = get_object_or_404(smodels.School, pk=id)
    response['Content-Disposition'] = 'attachment; filename=activity_report_school_%d.pdf' % school.id
    buffer = BytesIO()
    form = pdf.reports.ActivityReport(school)
    form.saveToPdf(buffer, request)
    # Get the value of the BytesIO buffer and write it to the response.
    data = buffer.getvalue()
    buffer.close()
    response.write(data)
    return response

@login_required
def csv_activity_report(request, id):
    response = http.HttpResponse(content_type='application/x-download')
    school = get_object_or_404(smodels.School, pk=id)
    response['Content-Disposition'] = 'attachment; filename=activity_report_school_%d.zip' % school.id
    form = CSVReport()
    form.addActivity(school, request)
    response.write(form.makeZip())
    return response

class SchoolList(UserMixin, ListView):
    model = smodels.School
    template_name = "schools/schools.html"
    context_object_name = "schools"
    def get_queryset(self):
        qs = super(SchoolList, self).get_queryset()
        if not self.request.user.is_superuser:
            qs = self.request.user.schools.all()
        return qs

def get_school_data(school):
    import time
    import stats.models as stmodels

    uses = stmodels.SchoolNumberComputersUsed.objects.filter(school=school)
    data_dict = {}
    # Avoid returning data if no use is found, so the template doesn't render
    # an empy graph
    if uses:
        for use in uses:
            timestamp = 1000 * int(time.mktime(use.day.timetuple()))
            try:
                data_dict[timestamp] += use.computers
            except:
                data_dict[timestamp] = use.computers
        data = []
        for key in data_dict:
            this_point = [key, data_dict[key]]
            data.append(this_point)
        return json.dumps(sorted(data))
    return None

class SchoolPermission(object):
    def get_object(self, queryset=None):
        obj = super(SchoolPermission, self).get_object(queryset)
        if not self.request.user.is_superuser:
            if obj not in self.request.user.schools.all():
                raise PermissionDenied
        return obj

class SchoolDetail(UserMixin, SchoolPermission, DetailView):
    model = smodels.School

    def get_context_data(self, **kwargs):
        ctx = super(SchoolDetail, self).get_context_data(**kwargs)
        alarmformset = pforms.AlarmFormSet(
            queryset=pmodels.Alarm.objects.filter(host__school=self.object, ack=False)
            )
        activityform = sforms.ActivityForm(prefix='activityform')
        scrapbookform_initial = {
            'school': self.object.pk,
            'user_id': self.request.user.id,
            }
        scrapbookform = sforms.SchoolScrapBookForm(prefix='scrapbookform', initial=scrapbookform_initial)
        registered_computers = self.object.computers.filter(dismissed=False)
        # Components statistics
        comps = stmodels.ComponentsStat.objects.filter(school=self.object).all()
        components_stats = []
        # Get all ComponentsStat.name values, to loop over components' types
        names = sorted(comps.values_list('name', flat=True).distinct())
        for name in names:
            this_type = comps.filter(name=name).order_by('-count')
            these_comps = []
            for comp in this_type:
                label = comp.value
                these_comps.append({'label': str(label), 'data': int(comp.count)})
            components_stats.append({str(name).replace("-", ""): these_comps})
        ctx.update({
            'school': self.object,
            'registered_computers': registered_computers,
            'activityform': activityform,
            'scrapbookform': scrapbookform,
            'alarmformset': alarmformset,
            'data': get_school_data(self.object),
            'components_stats': json.dumps(components_stats),
            'components_list': components_stats,
            })

        return ctx


@login_required
def add_scrapbook(request, id):
    # id is the school id
    school = get_object_or_404(smodels.School, pk=int(id))
    if not request.user.has_perm('schools.add_schoolscrapbook'):
        messages.error(request, _("You don't have the permissions to add a scrapbook"))
        return HttpResponseRedirect(reverse('school_detail', args=[school.id,]))
    scrapbookform = sforms.SchoolScrapBookForm(request.POST, prefix='scrapbookform')
    if scrapbookform.is_valid():
        scrapbook = scrapbookform.save(commit=False)
        scrapbook.user = request.user
        scrapbook.save()
    else:
        return HttpResponseRedirect(reverse('school_detail', args=[school.id,])+"#scrapbook")
    return HttpResponseRedirect(reverse('school_detail', args=[scrapbook.school.id,])+"#scrapbook")


@login_required
def create_other_hardware(request, id):
    # id is the school id
    school = get_object_or_404(smodels.School, pk=int(id))

    # Check that the user has permission to modify school, create
    # other_hardware, and that (s)he is the manager of the school
    if not (request.user.has_perm('schools.add_otherhardware') and school in request.user.schools.all()) and not request.user.is_superuser:
        messages.error(request, _('You cannot create other hardware'))
        return HttpResponseRedirect(reverse('school_detail', args=[school.id,]))

    if request.method == 'POST':
        otherhardwareform = sforms.OtherHardwareForm(request.POST)
        if otherhardwareform.is_valid():
            other_hardware = otherhardwareform.save(commit=False)
            other_hardware.school = school
            other_hardware.created_by = request.user
            other_hardware.save()
            return HttpResponseRedirect(reverse('school_detail', args=[school.id,])+"#otherhardware")
        else:
            return HttpResponseRedirect(reverse('school_detail', args=[school.id,])+"#otherhardware")
    else:
        initial_value = {
            'school': school.id,
            'kind': 1,
        }
        otherhardwareform = sforms.OtherHardwareForm(initial=initial_value)
    return render(request, 'schools/create_other_hardware.html', {
        'school': school,
        'form': otherhardwareform,
    })


@login_required
def edit_other_hardware(request, hw):
    # hw is the hardware id
    hw = get_object_or_404(smodels.OtherHardware, pk=int(hw))
    school = hw.school

    # Check that the user has permission to modify school, create
    # other_hardware, and that (s)he is the manager of the school
    if not (request.user.has_perm('schools.change_otherhardware') and school in request.user.schools.all()) and not request.user.is_superuser:
        messages.error(request, _('You cannot edit other hardware'))
        return HttpResponseRedirect(reverse('school_detail', args=[school.id,])+"#otherhardware")

    if request.method == 'POST':
        otherhardwareform = sforms.OtherHardwareForm(request.POST, instance=hw)
        if otherhardwareform.is_valid():
            other_hardware = otherhardwareform.save(commit=False)
            other_hardware.school = school
            other_hardware.save()
            return HttpResponseRedirect(reverse('school_detail', args=[school.id,])+"#otherhardware")

    otherhardwareform = sforms.OtherHardwareForm(instance=hw)

    return render(request, 'schools/create_other_hardware.html', {
        'editing': True,
        'school': school,
        'form': otherhardwareform,
        'object': hw
    })


class SchoolM2MMixin(object):
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        man = form.clean().get("managed")
        smodels.SchoolsManagers.objects.filter(school=self.object).delete()
        for x in man:
            smodels.SchoolsManagers.objects.create(school=self.object, manager=x)
        return redirect(self.get_success_url())

class SchoolCreate(UserMixin, SchoolM2MMixin, CreateView):
    perms = ['schools.add_school']
    model = smodels.School
    fields = (
        'name', 'area', 'lasis_code',
        'address', 'lat', 'lon',
        'phone_number',
        'email_address', 'managed'
        )

class SchoolUpdate(UserMixin, SchoolM2MMixin, UpdateView):
    perms = ['schools.change_school']
    model = smodels.School
    fields = (
        'name', 'area', 'lasis_code',
        'address', 'lat', 'lon',
        'phone_number',
        'email_address','managed', 'dismissed'
        )
    def get_success_url(self):
        if self.object.dismissed:
            messages.info(self.request, _('School {} has been dismissed, only admins can restore it'.format(self.object.name)))
            return reverse('schools')
        return super(SchoolUpdate, self).get_success_url()
@login_required
def delete_school(request, id):
    """
    Removes a school from the database.
    """
    if not request.user.has_perm('schools.delete_school'):
        messages.error(request, _(" You don't have the permission to delete this school"))
        return redirect('schools')
    school = get_object_or_404(smodels.School, pk=int(id))
    school_name = school.name
    school.delete()
    messages.info(request, _('School %s destroyed successfully' % school_name))
    return redirect('schools')


@login_required
def add_activity(request, id):
    # id is the school id
    school = get_object_or_404(smodels.School, pk=int(id))
    if not request.user.has_perm('schools.add_activity'):
        messages.error(request, _("You don't have the permission to add an activity"))
        return HttpResponseRedirect(reverse('school_detail', args=[id, ]))
    activityform = sforms.ActivityForm(request.POST, prefix='activityform')
    if activityform.is_valid():
        activity = activityform.save(commit=False)
        activity.user = request.user
        activity.school = school
        activity.save()
    else:
        return HttpResponseRedirect(reverse('school_detail', args=[school.id,])+"#activity")
    return HttpResponseRedirect(reverse('school_detail', args=[activity.school.id,])+"#activity")


@login_required
def edit_activity(request, id):
    # id is the activity id
    activity = get_object_or_404(smodels.Activity, pk=int(id))
    if not request.user.has_perm('schools.change_activity'):
        messages.error(request, _("You don't have the permissions to modify this activity"))
        return HttpResponseRedirect(reverse('school_detail', args=[activity.school.id,]))
    if request.method == 'POST':
        activityform = sforms.NewActivityForm(request.POST, instance=activity)
        if activityform.is_valid():
            activityform.save()
            return redirect('school_detail', pk=activity.school.id)
        else:
            pass
    else:
        activityform = sforms.NewActivityForm(instance=activity)
    return render(request, 'schools/edit_activity.html', {
        'form': activityform,
        'activity': activity,
    })


@login_required
def computers_dismissed(request, id):
    school = get_object_or_404(smodels.School, pk=int(id))
    dismissed_computers = school.computers.filter(dismissed=True)
    return render(request, 'schools/computers_dismissed.html', {
        'school': school,
        'dismissed_computers': dismissed_computers,
    })



@login_required
def get_schools_activities_json(request, id=None):
    # Collect GET variables
    sSearch = request.GET.get('sSearch', None)
    sEcho = request.GET.get('sEcho', None)
    iSortCol = request.GET.get('iSortCol_0', None)
    sSortDir = request.GET.get('sSortDir_0', None)
    iDisplayStart = request.GET.get('iDisplayStart', None)
    iDisplayLength = request.GET.get('iDisplayLength', None)
    zStartDate = request.GET.get('zStartDate', None)
    if zStartDate:
        # Convert from string to date object
        zStartDate = dt.datetime.strptime(zStartDate, '%Y-%m-%d').date()
        # Convert from date to datetime object
        zStartDate = dt.datetime.combine(zStartDate, dt.time(0, 0, 0))
    zEndDate = request.GET.get('zEndDate', None)
    if zEndDate:
        zEndDate = dt.datetime.strptime(zEndDate, '%Y-%m-%d').date()
        zEndDate = dt.datetime.combine(zEndDate, dt.time(23, 59, 59))
    fields = [
        'timestamp',
        'end_timestamp',
        'school',
        'user',
        'description',
    ]

    if id:
        # We are in the show_school() view
        school = get_object_or_404(smodels.School, pk=int(id))
        activities = smodels.Activity.objects.filter(school=school)
    else:
        if request.user.is_superuser:
            activities = smodels.Activity.objects.all()
        else:
            # We are in the search view, so get all the activities in the logged
            # user's schools
            activities = smodels.Activity.objects.filter(school__in=request.user.schools.all())

    iTotalRecords = activities.count()
    # Filter on GET variables
    if sSearch:
        activities = activities.filter(description__icontains=sSearch)
    if zStartDate:
        activities = activities.filter(timestamp__gte=zStartDate)
    if zEndDate:
        activities = activities.filter(timestamp__lte=zEndDate)
    activities_values = []
    iTotalDisplayRecords = activities.count()

    # Sort
    if iSortCol != '':
        if sSortDir == 'asc':
            activities = activities.order_by(fields[int(iSortCol)])
        else:
            activities = activities.order_by("-%s" % fields[int(iSortCol)])

    # Slice
    start = int(iDisplayStart)
    length = int(iDisplayLength)

    for activity in activities[start:start+length]:
        row = []
        row.append("%s" % activity.timestamp.strftime('%d/%m/%Y %H:%M:%S'))
        row.append("%s" % activity.end_timestamp.strftime('%d/%m/%Y %H:%M:%S'))
        row.append("%s" % activity.school)
        row.append("%s" % activity.user)
        row.append("%s" % activity.description)
        if request.user.has_perm('schools.change_activity'):
            row.append(
            """
            <a href="%s">%s</a>
            """ % (str(reverse('edit_activity', args=[activity.id,])), 'Edit activity'))
        else:
            row.append("")
        activities_values.append(row)

    # Prepare JSON response
    response = json.dumps({
        'iTotalRecords': iTotalRecords,
        'iTotalDisplayRecords': iTotalDisplayRecords,
        'sEcho': sEcho,
        'aaData': activities_values,
    })
    return HttpResponse(response, content_type='application/json')


def search_activities(request):
    return render(request, 'schools/search_activities.html')


@login_required
def school_log(request):
    if not request.user.is_superuser:
        messages.error(request, _("You don't have the permissions to see this data"))
        return HttpResponseRedirect(reverse('home'))
    _data = smodels.SchoolKeyUsage.objects.filter(school__dismissed=False).values("school__name", "school__pk").annotate(last=Max("timestamp")).order_by("last")
    data = []
    for x in _data:
        school = smodels.School.objects.get(pk=x["school__pk"])
        class School(object):
            school__name = x["school__name"]
            school__pk = x["school__pk"]
            last = x["last"]
            get_non_acked_alarms = school.get_non_acked_alarms
        data.append(School())

    return render(request, 'schools/school_log.html', {
        'data': data
    })


class DropOtherHardware(UserMixin, DeleteView):
    model = smodels.OtherHardware
    template_name = "schools/drop_hardware.html"

    def get_success_url(self, **kw):
        return reverse('school_detail', kwargs={'pk': self.object.school.pk})
