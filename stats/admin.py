# coding: utf-8
# Automatically registers models in the admin
# Snippet found here: http://djangosnippets.org/snippets/2066/ and slightly
# modified
from django.contrib import admin
from stats import models as smodels

admin.site.register(smodels.SchoolNumberComputersUsed)
admin.site.register(smodels.ComponentsStat)
