# coding: utf-8

components_stats_to_be_kept = [
    'cpuspeed',
    'cputype',
    'fussclient',
    'fuss-server',
    'octofuss-client',
    'octomon-sender',
    'octofussd',
    'octonet',
    'system_vendor',
    'totalram',
    'video_size',
    'videocard',
    '__version__',
]
