# coding: utf-8
from django.core.management.base import BaseCommand
from django.utils.translation import ugettext_lazy as _
import datetime
import schools.models as scmodels
import stats.models as stmodels
from stats.const import components_stats_to_be_kept


class Command(BaseCommand):
    args = '<yyyy-mm-dd>'
    help = _('Updates the daily used computers statistics for the chosen day, aggregated by schools.')

    def handle(self, *args, **options):
        stmodels.ComponentsStat.objects.all().delete()
        how_many_schools = 0
        how_many_components = 0
        how_many_global = 0
        day = None
        # Aggregated statistics about schools
        print "Updating aggregated statistics about schools..."
        schools = scmodels.School.objects.all()
        if len(args) == 1:
            day = datetime.datetime.strptime(args[0], '%Y-%m-%d').date()
            beginning_of_the_day = datetime.datetime.combine(day, datetime.time(0, 0, 0))
            end_of_the_day = datetime.datetime.combine(day, datetime.time(23, 59, 59))
            print type(day), day
            for school in schools:
                count = school.computers.filter(uses__timestamp__range=(beginning_of_the_day, end_of_the_day)).distinct().count()
                obj, created = stmodels.SchoolNumberComputersUsed.objects.get_or_create(
                    school=school,
                    day=day
                )
                obj.computers = count
                obj.save()
                print school, count
                how_many_schools += 1
        if len(args) == 0:
            for school in schools:
                print "SCHOOL:", school.name
                data = school.count_daily_computers_used()
                for _day in data:
                    obj, created = stmodels.SchoolNumberComputersUsed.objects.get_or_create(
                        school=school,
                        day=_day,
                        computers=data[_day],
                    )
                    obj.save()
                    how_many_schools += 1
        print "Updating statistics about components..."
        # Statistics about components
        schools = scmodels.School.objects.all()
        # Prepare global_stats
        global_stats = dict()
        for x in components_stats_to_be_kept:
            global_stats[x] = dict()
        for school in schools:
            stats = school.get_components_stats()
            for name in stats:
                for value in stats[name]:
                    obj, created = stmodels.ComponentsStat.objects.get_or_create(
                        school=school,
                        name=name,
                        value=value,
                    )
                    obj.count=stats[name][value]
                    obj.save()
                    actual_count = int(global_stats.get(name).get(value, 0))
                    global_stats[name][value] = actual_count + obj.count
                    how_many_components += 1
        # Save global stats in the database
        # School = None
        for name in global_stats:
            for value in global_stats[name]:
                obj, created = stmodels.ComponentsStat.objects.get_or_create(
                    school=None,
                    name=name,
                    value=value,
                )
                obj.count=global_stats[name][value]
                obj.save()
                how_many_global += 1
        print "Saved %d schools' records" % how_many_schools
        print "Saved %d components' records" % how_many_components
        print "Saved %d global components' records" % how_many_global
        print 'OK.'
