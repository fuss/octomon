# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schools', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ComponentsStat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=200, blank=True)),
                ('value', models.CharField(default=b'', max_length=200, blank=True)),
                ('count', models.IntegerField(default=0)),
                ('timestamp', models.DateTimeField(auto_now=True)),
                ('school', models.ForeignKey(related_name='components_stats', to='schools.School', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='SchoolNumberComputersUsed',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('day', models.DateField()),
                ('computers', models.IntegerField(default=0)),
                ('school', models.ForeignKey(related_name='computer_daily_uses', to='schools.School')),
            ],
            options={
                'verbose_name_plural': 'Schools number computers used',
            },
        ),
        migrations.AlterUniqueTogether(
            name='schoolnumbercomputersused',
            unique_together=set([('day', 'school')]),
        ),
    ]
