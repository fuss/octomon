from django.db import models

class SchoolNumberComputersUsed(models.Model):
    """
    Model that holds the number of computers used in a school in a day
    """
    day = models.DateField()
    school = models.ForeignKey('schools.School', related_name='computer_daily_uses')
    computers = models.IntegerField(default=0)

    class Meta:
        verbose_name_plural = 'Schools number computers used'
        unique_together = ('day', 'school')


class ComponentsStat(models.Model):
    school = models.ForeignKey('schools.School', related_name='components_stats', null=True)
    name = models.CharField(max_length=200, blank=True, default='')
    value = models.CharField(max_length=200, blank=True, default='')
    count = models.IntegerField(default=0)
    timestamp = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        if self.school is not None:
            school = self.school.name
        else:
            school = ""
        return "%s - %s[%s]: %d" % (
            school, self.name, self.value, self.count
        )
