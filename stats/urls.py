from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^$', 'stats.views.general_statistics', name="general_statistics"),
    url(r'^activities_summary/$', 'stats.views.activities_summary', name="activities_summary"),
    url(r'^computer_distribution/$', 'stats.views.computer_distribution', name="computer_distribution"),
    url(r'^computer_usage/$', 'stats.views.computer_usage', name="computer_usage"),
    url(r'^user_distribution/$', 'stats.views.user_distribution', name="user_distribution"),
    url(r'^components/$', 'stats.views.components_stats', name="components_stats"),
)
