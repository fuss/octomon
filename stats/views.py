from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from operator import itemgetter
import json
import schools.models as scmodels
import stats.models as stmodels
import hosts.models as hmodels


@login_required
def general_statistics(request):
    return redirect('computer_distribution')


@login_required
def activities_summary(request):
    schools = scmodels.School.objects.all()
    schools_stats = []
    for school in schools:
        this_school = {
            'school_id': school.id,
            'school_name': school.name,
            'address': school.address,
            'total_activity_hours': school.get_total_activity_hours(),
        }
        schools_stats.append(this_school)
    return render(request, 'stats/activities_summary.html', {
        'schools_stats': sorted(schools_stats, key=itemgetter('total_activity_hours'), reverse=True),
    })


@login_required
def computer_distribution(request):
    total_computers = hmodels.Computer.objects.all().count()
    total_active_computers = hmodels.Computer.objects.filter(dismissed=False).count()

    schools_stats = []

    schools = scmodels.School.objects.all()
    for school in schools:
        this_school = {
            'school_id': school.id,
            'school_name': school.name,
            'address': school.address,
            'computers': school.count_active_computers()
        }
        this_school['percentage'] = 0
        if total_active_computers and this_school['computers']:
            this_school['percentage'] = 100 * this_school['computers'] / total_active_computers

        schools_stats.append(this_school)
    return render(request, 'stats/computer_distribution.html', {
        'schools_stats': sorted(schools_stats, key=itemgetter('computers'), reverse=True),
        'total_computers': total_computers,
        'total_active_computers': total_active_computers,
    })


@login_required
def computer_usage(request):
    import time
    uses = stmodels.SchoolNumberComputersUsed.objects.all()
    data_dict = {}
    for use in uses:
        timestamp = 1000 * int(time.mktime(use.day.timetuple()))
        try:
            data_dict[timestamp] += use.computers
        except:
            data_dict[timestamp] = use.computers
    data = []
    for key in data_dict:
        this_point = [key, data_dict[key]]
        data.append(this_point)
    return render(request, 'stats/computer_usage.html', {
        'data': json.dumps(sorted(data)),
    })


@login_required
def user_distribution(request):
    general_stats = {
        'total_users': 0,
        'total_computers': 0,
    }
    average_distribution_list = []

    schools = scmodels.School.objects.all()
    for school in schools:
        general_stats['total_users'] += school.get_total_users()

    schools_stats = []
    for school in schools:
        this_school = {
            'users': school.get_total_users(),
            'computers': school.count_computers(),
        }
        general_stats['total_computers'] += this_school['computers']
        if this_school['computers'] > 0:
            this_school['ratio'] = "%.2f" % (this_school['users'] / float(this_school['computers']))
            average_distribution_list.append(float(this_school['ratio']))
        else:
            this_school['ratio'] = "0"
        this_school['school_name'] = school.name
        this_school['school'] = school
        this_school['school_id'] = school.id
        this_school['address'] = school.address
        this_school['percentage'] = 0
        if this_school['users'] and general_stats['total_users']:
            this_school['percentage'] = 100 * this_school['users'] / general_stats['total_users']

        schools_stats.append(this_school)
    general_stats['ratio'] = 0
    if average_distribution_list:
        general_stats['ratio'] = "%.2f" % (sum(average_distribution_list) / float(len(average_distribution_list)))

    return render(request, 'stats/user_distribution.html', {
        'general_stats': general_stats,
        'schools_stats': sorted(schools_stats, key=itemgetter('users'), reverse=True),
    })


@login_required
def components_stats(request):
    # Components statistics
    comps = stmodels.ComponentsStat.objects.filter(school__isnull=True)
    components_stats = []
    # Get all ComponentsStat.name values, to loop over components' types
    names = sorted(comps.values_list('name', flat=True).distinct())
    for name in names:
        this_type = comps.filter(name=name).order_by('-count')
        these_comps = []
        for comp in this_type:
            label = comp.value
            these_comps.append({'label': str(label), 'data': int(comp.count)})
        components_stats.append({str(name).replace("-", ""): these_comps})
    return render(request, 'stats/components_statistics.html', {
        'components_stats': json.dumps(components_stats),
        'components_list': components_stats,
    })
