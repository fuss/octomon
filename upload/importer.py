#coding=utf-8
from backend import Encoder, Decoder, Queue, OctomonMessage, prettyFingerprint
from django.core.exceptions import ObjectDoesNotExist
from schools import models as smodels
from hosts import models as hmodels

from datetime import datetime
import re

from django.conf import settings

inbound_queue = getattr(settings, "INBOUND_QUEUE", "inbound")
done_queue = getattr(settings, "DONE_QUEUE", "imported")
cert_priv = getattr(settings, "CERT_PRIV", "certs/server_key.pem")
cert_pub = getattr(settings, "CERT_PUB", "certs/server.pem")

class Importer:
    def __init__(self):
        self.newQueue = Queue(inbound_queue)
        self.doneQueue = Queue(done_queue)

    def importMessage(self, msg, sk):
        """
        Import an octomon.Message using the given SchoolKey
        """

        # print "Metadata:"
        # for line in msg.metadataFile():
        #    print "  ", line.strip()
        start_ts = 0
        end_ts = 0
		
        school = sk.school
        maxts = school.last_timestamp
        ip = re.compile(r"^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$")
        SERVER = None
        for host, key, val, ts in msg.tuples():
            if start_ts == 0:
                start_ts = ts
            if end_ts == 0:
                end_ts = ts
            if ts > end_ts:
                end_ts = ts
            if ts < start_ts:
                start_ts = ts
				
            if ts <= school.last_timestamp:
                # Do not reimport old data
                #print "Old: skipped"
                continue
            if ip.match(host):
                # Do not import data from IP addresses
                #print "IP: skipped", host
                continue
            computer = hmodels.Computer.objects.filter(school=school, hostname=host)
            computer = [x for x in computer]
            if len(computer) == 0:
                computer = hmodels.Computer(school=school, hostname=host, dismissed=False)
                computer.save()
            else:
                computer = computer[0]

            if key == "START":
                #print "Usage->Computer:", computer
                new_i = hmodels.Use(host=computer, timestamp=datetime.fromtimestamp(ts))
                new_i.save()
            else:
                #print "Components->Computer:", computer
                if key == "fuss-client":
                    key = "fussclient"
                if key == "is_server":
                    if val == "yes":
                        if not SERVER:
                            SERVER = computer
                new_i = hmodels.Component(name=key, value=val, host=computer, timestamp=datetime.fromtimestamp(ts))
                new_i.save()
            if ts > maxts: maxts = ts
        if SERVER:
            # Move data from server hostname to :SERVER: refs #622
            try:
                real_server = hmodels.Computer.objects.get(
                    school=SERVER.school,
                    hostname=":SERVER:"
                )
            except:
                real_server = hmodels.Computer.objects.create(
                    school=SERVER.school,
                    hostname=":SERVER:"
                )
            for c in hmodels.Component.objects.filter(host=SERVER):
                c.host = real_server
                c.save()
            SERVER.delete()
        school.last_timestamp = ts
        school.save()
        u = smodels.SchoolKeyUsage(school=school, key=sk, timestamp=datetime.now(),
						   last_data_timestamp = end_ts,
						   first_data_timestamp = start_ts)
        u.save()
        # Create total_users schoolprop if it doesn't exist
        q = hmodels.Component.objects.filter(host__school=school, name="total_users").order_by("-timestamp")
        if len(q) > 0:
            # Last timestamp should be at [0]
            try:
                obj = smodels.SchoolProp.objects.get(school=school, propname="total_users")
                obj.propvalue = q[0].value
                obj.save()
            except:
                obj = smodels.SchoolProp.objects.create(propname="total_users", school=school, propvalue=q[0].value)

        # Drop ghost computers. refs #629
        for host in hmodels.Computer.objects.filter(school=school):
            if len(hmodels.Component.objects.filter(host=host)) == 0:
                host.delete()

		
    def queueRun(self):
        """
        Go through every message in the queue; if their fingerprint has been
        approved, import them
        """
        for name in self.newQueue.list():
            cleartext = self.newQueue.read(name)
            msg = OctomonMessage(cleartext)
            try:
                sk = smodels.SchoolKey.objects.get(fingerprint = msg.fingerprint())
                self.importMessage(msg, sk)
                self.doneQueue.add(cleartext)
                self.newQueue.remove(name)
            except ObjectDoesNotExist:
                pass

    def acquireMessage(self, cleartext, msg=None):
        """
        If the message fingerprint is trusted, import it.  Else, enqueue it in
        the approval queue.

        Returns True if the message has been imported, False if it has been
        enqueued.
        """
        if msg is None:
            msg = OctomonMessage(cleartext)
        try:
            sk = smodels.SchoolKey.objects.get(fingerprint=msg.fingerprint())
        except ObjectDoesNotExist:
            # If the key was not found, enqueue waiting to be validated
            self.newQueue.add(cleartext)
            return False

        self.importMessage(msg, sk)
        self.doneQueue.add(cleartext)
        return True
