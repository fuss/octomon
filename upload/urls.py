from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^queue$', 'upload.views.queue', name="upload_queue"),
    url(r'^form$', 'upload.views.form', name="upload_form"),
    url(r'^approve$', 'upload.views.approve', name="upload_approve"),
    url(r'^remove$', 'upload.views.remove', name="upload_remove"),
    url(r'^schooldata$', 'upload.views.schooldata', name="upload_schooldata"),
)
