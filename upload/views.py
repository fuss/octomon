# coding: utf-8
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponseBadRequest
from django.http import HttpResponse
from schools import models as smodels
from backend import Encoder, Decoder, Queue, OctomonMessage, prettyFingerprint
from importer import Importer
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

cert_priv = getattr(settings, "CERT_PRIV", "certs/server_key.pem")
cert_pub = getattr(settings, "CERT_PUB", "certs/server.pem")

my_importer = Importer()

@login_required
def form(request):
    return render(request, 'upload/form.html')

@login_required
def queue(request):
    """
    List all elements in the queue
    """
    names = []
    for x in my_importer.newQueue.list():
        msg = OctomonMessage(my_importer.newQueue.read(x))
        certinfo = msg.certinfo()["_subject"]
        names.append(dict(item=x,
                          msg=msg,
                          certinfo=certinfo,
                          fingerprint=prettyFingerprint(msg.fingerprint())))


    schools = smodels.School.objects.all()
    return render(request, 'upload/queue.html', dict(names=names,
                                                     schools=schools))

@login_required
def approve(request):
    """
    Approve one file in the queue, and perform a queue run
    """
    name = request.POST.get("name")
    school = get_object_or_404(smodels.School, pk=request.POST.get("school"))

    if not my_importer.newQueue.validate(name):
        return HttpResponseBadRequest("NOT VALID DATA")
    msg = OctomonMessage(my_importer.newQueue.read(name))
    try:
        sk = smodels.SchoolKey.objects.get(fingerprint=msg.fingerprint())
    except:
        sk = smodels.SchoolKey(school=school,
                               fingerprint=msg.fingerprint(),
                               crypto_key=msg.certificate())
        sk.save()

    my_importer.queueRun()
    return redirect('upload_queue')

@login_required
def remove(request):
    """
    Remove item from the queue
    """
    name = request.POST.get('name')
    if not my_importer.newQueue.validate(name):
        return HttpResponseBadRequest("NOT VALID DATA")
    my_importer.newQueue.remove(name)
    return redirect('upload_queue')


# NO LOGIN REQUIRED! DON'T ADD IT!
@csrf_exempt
def schooldata(request):
    """
    Upload school data
    """

    file = request.FILES.get("file", None)
    if not file:
        return HttpResponseBadRequest("Missing payload")

    try:
        decoder = Decoder(cert_pub, cert_priv)
        decoded = decoder.decode(file.read())
        msg = OctomonMessage(decoded)
    except Exception as e:
        return HttpResponseBadRequest("Bad Request: %s" % str(e))

    if my_importer.acquireMessage(decoded, msg):
        return HttpResponse("Message imported")
    else:
        from django.core.mail import mail_admins

        mail_admins("New unknown payload",
                    """A new data payload with unknown certificate has been received.
                    Please check the upload queue""")
        return HttpResponse("Message enqueued")
